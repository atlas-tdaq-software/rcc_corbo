/********************************************************/
/* file: iom_corbo_int.h				*/
/*							*/
/*  5. Dez. 00  MAJO  created				*/
/*  8. Mar. 01  MAJO  PCI and VMEbus versions merged    */
/*							*/
/****C 2010 - The software with that certain something***/

#ifndef _IOM_CB_INT_H
#define _IOM_CB_INT_H


#include <sys/types.h>

/*generic definitions*/
#define VME 0
#define PCI 1 

/* definitions for the VMEbus CORBO*/

typedef struct
{
  u_char dummy;
  u_char data;
} T_bim;

typedef struct
{
  u_short low;
  u_short high;
} T_evcnt;

typedef struct
{
  u_short csr[4];    /*0x00-0x07*/
  u_char  d1[8];     /*0x08-0x0f*/
  T_evcnt        evcnt[4];  /*0x10-0x1f*/
  u_short dtime[4];  /*0x20-0x27*/
  u_char  d2[8];     /*0x28-0x2f*/
  T_bim          bim1cr[4]; /*0x30-0x38*/
  T_bim          bim1vr[4]; /*0x39-0x3f*/
  T_bim          bim2cr[4]; /*0x40-0x47*/
  T_bim          bim2vr[4]; /*0x48-0x4f*/
  u_short test[4];   /*0x50-0x57*/
  u_short clear[4];  /*0x58-0x5f*/
  u_short ram[160];  /*0x60-0xff*/ 
} T_vme_corbo;

#define	VMETR_CSR1	0X00	// status registers
#define	VMETR_CSR2	0X02
#define	VMETR_CSR3	0X04
#define	VMETR_CSR4	0X06

#define	VMETR_CNT1	0X10	// counter
#define	VMETR_CNT2	0X14
#define	VMETR_CNT3	0X18
#define	VMETR_CNT4	0X1C

#define	VMETR_TOU1	0X20	// time out
#define	VMETR_TOU2	0X22
#define	VMETR_TOU3	0X24
#define	VMETR_TOU4	0X26

#define	VMETR_BIM1	0X30	// BIM
#define	VMETR_BIM2	0X40
#define OFST_CR0	0X01	// control
#define OFST_CR1	0X03
#define OFST_CR2	0X05
#define OFST_CR3	0X07
#define OFST_VR0	0X09	// vector
#define OFST_VR1	0X0B
#define OFST_VR2	0X0D
#define OFST_VR3	0X0F

#define	VMETR_TEST1	0X50	// test (to generate)
#define	VMETR_TEST2	0X52
#define	VMETR_TEST3	0X54
#define	VMETR_TEST4	0X56

#define	VMETR_CLEAR1	0X58	// clear
#define	VMETR_CLEAR2	0X5A
#define	VMETR_CLEAR3	0X5C
#define	VMETR_CLEAR4	0X5E

#define	VMETR_RAM	0X60	// RAM

#define VMETR_AM	0x39
#define	VMETR_LEN	0x100

/*definitions for the PCI CORBO*/
/* csr: Control and Status Register*/
/* evcnt: Event counter (busy or trigger)*/
/* irqcsr: Event Interrupt Control Register*/
/* test: Test Pulse*/
/* clear: Clear Busy Pulse*/
typedef struct
{
  u_int csr;       /*0x00*/
  u_int evcnt;     /*0x04*/
  u_int irqcsr;    /*0x08*/
  u_int test;      /*0x0c*/
  u_int clear;     /*0x10*/
  u_int d1[11];    /*0x14-0x3c*/
} T_corbo_chan;

typedef struct
{
  u_int version;   /*0x00*/
  u_int d1[3];     /*0x04-0x0c*/
  u_int polarity;  /*0x10*/
  u_int d2[3];     /*0x14-0x1c*/
  u_int irqpend;   /*0x20*/
  u_int d3[7];     /*0x24-0x3c*/
  T_corbo_chan chan[4];   /*0x40-0x13c*/
} T_pci_corbo;

typedef struct
{
  u_int dummy[26]; /*0x00-0x64*/
  u_int intcsr;    /*0x68*/  
} T_plx;

#define PCI_CORBO_VENDOR  0x10b5
#define PCI_CORBO_DEVICE  0x9080
#define CORBO_VERSION     0x04810479
#define VERSION_MASK      0x00003c00
#define REVISION_MASK     0x000003e0

/*Corbo Interrupt control bits*/
#define CORBO_INTCR_IRAC  0x00000008 /*acknowledge*/
#define CORBO_INTCR_IRE   0x00000010 /*enable*/
#define CORBO_INTCR_FAC   0x00000040 /*Flag auto clear*/
#define CORBO_INTCR_FLAG  0x00000080 /*Test and set flag*/

/*Corbo status and control bits*/
#define CORBO_CSR_ENABLE  0x00000001 /*enable channel*/
#define CORBO_CSR_INSEL   0x00000004 /*?*/
#define CORBO_CSR_CNTSEL  0x00000020 /*?*/
#define CORBO_CSR_INSTATE 0x00000100 /*?*/
#define CORBO_CSR_BSSTATE 0x00000200 /*?*/
#define CORBO_CSR_INTPEND 0x00000800 /*interrupt pending*/

/*Corbo global interrupt status (register irqpend)*/
#define CORBO_IRQPEND0    0x00000001
#define CORBO_IRQPEND1    0x00000002
#define CORBO_IRQPEND2    0x00000004
#define CORBO_IRQPEND3    0x00000008

/*Polarities of trigger input and busy output*/
#define CORBO_TRIGPOL0_AH 0x00000001  /*Active high*/
#define CORBO_BUSYPOL0_AH 0x00000002  /*Active high*/
#define CORBO_TRIGPOL1_AH 0x00000004  /*Active high*/
#define CORBO_BUSYPOL1_AH 0x00000008  /*Active high*/
#define CORBO_TRIGPOL2_AH 0x00000010  /*Active high*/
#define CORBO_BUSYPOL2_AH 0x00000020  /*Active high*/
#define CORBO_TRIGPOL3_AH 0x00000040  /*Active high*/
#define CORBO_BUSYPOL3_AH 0x00000080  /*Active high*/
#define CORBO_TRIGPOL0_AL 0x00000001  /*Active low*/
#define CORBO_BUSYPOL0_AL 0x00000002  /*Active low*/
#define CORBO_TRIGPOL1_AL 0x00000004  /*Active low*/
#define CORBO_BUSYPOL1_AL 0x00000008  /*Active low*/
#define CORBO_TRIGPOL2_AL 0x00000010  /*Active low*/
#define CORBO_BUSYPOL2_AL 0x00000020  /*Active low*/
#define CORBO_TRIGPOL3_AL 0x00000040  /*Active low*/
#define CORBO_BUSYPOL3_AL 0x00000080  /*Active low*/

#endif
