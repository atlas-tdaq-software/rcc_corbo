/*
* file: iom_corbo.h
*
* 930622 MAWE change base address to f0600000
* 931006 MAJO change base address to df700000
* 960725 JOP  add prototypes
* 000121 JOP IOM compatibility 
*/


#ifndef _IOM_CB_H
#define _IOM_CB_H

#include <sys/types.h>
#include "rcc_error/rcc_error.h"

enum cb_errors 
{
  CBE_OK,
  CBE_ISOPEN = (P_ID_CORBO << 8)+1,
  CBE_ISNOTOPEN,
  CBE_VMEOPEN,
  CBE_VMEMSTMAP,
  CBE_VMESLVMAP,
  CBE_VMEMSTUNMAP,
  CBE_VMECLOSE,
  CBE_NOSUPP,
  CBE_IORCC,
  CBE_NO_CODE,
  CBE_ERROR_FAIL
};

#define CBE_OK_STR          "OK"
#define CBE_ISOPEN_STR      "Corbo library already open"
#define CBE_ISNOTOPEN_STR   "Corbo library not opened"
#define CBE_VMEOPEN_STR     "Error on VME_Open"
#define CBE_VMEMSTMAP_STR   "Error on VME_MasterMap"
#define CBE_VMESLVMAP_STR   "Error on VME_SlvMap"
#define CBE_VMEMSTUNMAP_STR "Error on VME_MasterUnmap"
#define CBE_VMECLOSE_STR    "Error on VME_Close"
#define CBE_NOSUPP_STR      "This function is not supported"
#define CBE_IORCC_STR       "Error on function of io_rcc"
#define CBE_NO_CODE_STR     "Unknown error code"
#define CBE_ERROR_FAIL_STR  "Cannot open/close error pack"

#ifdef __cplusplus
extern "C" {
#endif

// prototypes
err_type TM_open(u_int vme_addr);
err_type TM_close();
err_type TM_csrW(int channel,int value);
err_type TM_csrR(int channel,int *value);
err_type TM_cntW(int channel, int value);
err_type TM_cntR(int channel, int *value);
err_type TM_toW(int channel, int value);
err_type TM_toR(int channel, int *value);
err_type TM_bimcrW(int channel, int value);
err_type TM_bimcrR(int channel, int *value);
err_type TM_bimvrW(int channel, int value);
err_type TM_bimvrR(int channel, int *value);
err_type TM_test(int channel);
err_type TM_clbsy(int channel);
err_type TM_ramW(int offset, int value);
err_type TM_ramR(int offset, int *value);
err_type TM_etc(int channel, int function, int *iodata);
err_type TM_bim2vrR(int channel, int *value);
err_type TM_bim2vrW(int channel, int value);
err_type TM_bim2crW(int channel,int value);
err_type TM_bim2crR(int channel,int *value);

#ifdef __cplusplus
}
#endif

#endif
