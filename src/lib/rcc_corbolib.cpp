/**************************************************************************/
/*   file: corbolib.c							  */
/*									  */
/*   library for the VME trigger module (Ph.Farthouat, P.Gallno  ECP/EDU) */
/*   and the microEnable based PCI version of A. Kugel.			  */
/*									  */
/*    8. Feb. 92  JOP   Created						  */
/*   20. Nov. 95  JOP   Adaptation for Motorola MVME1600 Blaise		  */
/*   19. Apr. 99  MAJO  ported to Linux					  */
/*   21. Jan. 00  JOP   make an IOM package  & adapt to Linux		  */
/*    8. Mar. 01  MAJO  VMEbus and PCI versions merged			  */
/*									  */
/**************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "io_rcc/io_rcc.h"
#include "DFDebug/DFDebug.h"
#include "rcc_corbo.h"
#include "rcc_corbo_int.h"

#define OPEN_CHECK { if (open_count<1) return (RCC_ERROR_RETURN (0, CBE_ISNOTOPEN)); }


// Global def of the error function
static err_type cb_err_get(err_pack err, err_str pid, err_str code);

// Globals 
static int open_count = 0, mhandle;
static u_long base_add = 0, hwtype, cbase, pbase;
static u_int uhandle;
T_vme_corbo *vcorbo;
T_pci_corbo *pcorbo;
T_plx *plx;
static VME_MasterMap_t master_map;


/*********************************/
err_type TM_open(u_int vme_address)
/*********************************/
{
  err_type c_stat;
  u_int plxbase, corbobase;

  DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_open called");
  //  multiple opens
  if (open_count) 
  {
    open_count++;
    return(RCC_ERROR_RETURN(0, CBE_ISOPEN));
  }

  c_stat = iom_error_init(P_ID_CORBO, cb_err_get);
  if (c_stat != 0) 
    return(RCC_ERROR_RETURN(0, CBE_ERROR_FAIL));

  //Lets find out if the CORBO is PCI or VMEbus based

  c_stat = IO_Open();
  if (c_stat)
    return(RCC_ERROR_RETURN(c_stat, CBE_IORCC));

  c_stat = IO_PCIDeviceLink(PCI_CORBO_VENDOR, PCI_CORBO_DEVICE, 1, &uhandle);
  if (c_stat)
    hwtype = VME;
  else
    hwtype = PCI;

  if(hwtype == VME)
  {
    c_stat = VME_Open();
    if (c_stat)
      return (RCC_ERROR_RETURN (c_stat, CBE_VMEOPEN));

    master_map.vmebus_address   = vme_address;
    master_map.window_size      = 0x1000;
    master_map.address_modifier = VME_A24;
    master_map.options          = 0;
    c_stat = VME_MasterMap(&master_map, &mhandle);
    if (c_stat != VME_SUCCESS)
      return (RCC_ERROR_RETURN (c_stat, CBE_VMEMSTMAP));

    c_stat = VME_MasterMapVirtualLongAddress(mhandle, &base_add);
    if (c_stat != VME_SUCCESS)
      return (RCC_ERROR_RETURN (c_stat, CBE_VMEMSTMAP));

    vcorbo = (T_vme_corbo *)base_add; 
  }

  if(hwtype == PCI)
  {
    c_stat = IO_PCIConfigReadUInt(uhandle, 0x10, &plxbase);  //PLX registers are in BAR0
    if (c_stat)
      return(RCC_ERROR_RETURN(c_stat, CBE_IORCC));

    c_stat = IO_PCIConfigReadUInt(uhandle, 0x18, &corbobase);  //CORBO registers are in BAR2
    if (c_stat)
      return(RCC_ERROR_RETURN(c_stat, CBE_IORCC));

    c_stat = IO_PCIMemMap(plxbase, 0x1000, &pbase);  
    if (c_stat) 
      return(RCC_ERROR_RETURN(c_stat, CBE_IORCC));
    plx = (T_plx *)pbase;
    DEBUG_TEXT(DFDB_RCCCORBO, 20, "TM_open: PLX intcsr is " << HEX(plx->intcsr) << " at " << HEX((u_long)&plx->intcsr));

    c_stat = IO_PCIMemMap(corbobase, 0x1000, &cbase);  
    if (c_stat) 
      return(RCC_ERROR_RETURN(0, CBE_IORCC));
    pcorbo = (T_pci_corbo *)cbase;
    DEBUG_TEXT(DFDB_RCCCORBO, 20, "TM_open: CORBO registers are at PCI address " << HEX(corbobase));
  }

  open_count = 1;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


/*********************/
err_type TM_close(void)
/*********************/
{
  err_type c_stat;

  OPEN_CHECK

  if (open_count > 1)
  {
    open_count-- ;
    return (RCC_ERROR_RETURN (0, CBE_OK));
  }

  if(hwtype == VME)
  {
    c_stat = VME_MasterUnmap(mhandle);
    if (c_stat)
      return (RCC_ERROR_RETURN (c_stat, CBE_VMESLVMAP));

    c_stat = VME_Close();
    if (c_stat)
      return (RCC_ERROR_RETURN (c_stat, CBE_VMECLOSE));
  }

  if(hwtype == PCI)
  {
    c_stat = IO_PCIMemUnmap(cbase, 0x1000);
    if (c_stat)
      return(RCC_ERROR_RETURN(0, CBE_IORCC));

    c_stat = IO_PCIMemUnmap(pbase, 0x1000);
    if (c_stat)
      return(RCC_ERROR_RETURN(0, CBE_IORCC));

    c_stat = IO_PCIDeviceUnlink(uhandle);
    if (c_stat)
      return(RCC_ERROR_RETURN(0, CBE_IORCC));  
    
    c_stat = IO_Close();
    if (c_stat)
      return(RCC_ERROR_RETURN(0, CBE_IORCC));
  }

  open_count = 0;
  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(16 bit) into CSR of channel 'channel'
/**************************************/
err_type TM_csrW(int channel, int value)
/**************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_csrW: writing value " << HEX(value));
    vcorbo->csr[channel - 1] = value;
  }
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].csr = (u_int) value;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(12 bit) from CSR of channel 'channel'
/***************************************/
err_type TM_csrR(int channel, int *value)
/***************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->csr[channel - 1];
  if (hwtype == PCI)
    *value = pcorbo->chan[channel - 1].csr & 0xfff;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(2*16 bit) into counter of channel 'channel'
/**************************************/
err_type TM_cntW(int channel, int value)
/**************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_cntW: writing value " << HEX(value));
    vcorbo->evcnt[channel - 1].low = value >> 16;
    vcorbo->evcnt[channel - 1].high = value & 0xffff;
  } 
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].evcnt = (u_int)value;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(2*16 bit) from counter of channel 'channel'
/***************************************/
err_type TM_cntR(int channel, int *value)
/***************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
   { 
    *value = (vcorbo->evcnt[channel - 1].low) << 16;
    *value |= vcorbo->evcnt[channel - 1].high;
   } 
  if (hwtype == PCI)
    *value = pcorbo->chan[channel - 1].evcnt;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(16 bit) to timeout register  of channel 'channel'
/*************************************/
err_type TM_toW(int channel, int value)
/*************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_toW: writing value " << HEX(value));
    vcorbo->dtime[channel - 1] = value;
  }
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(16 bit) from timeout register  of channel 'channel'
/**************************************/
err_type TM_toR(int channel, int *value)
/**************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->dtime[channel - 1];
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(byte !!) into BIM1 CR of channel 'channel'
/****************************************/
err_type TM_bimcrW(int channel, int value)
/****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_bimcrW: writing value " << HEX(value));
    vcorbo->bim1cr[channel - 1].data = value;
  }
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].irqcsr = (u_int)value & 0xff;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(byte) from BIM1 CR register  of channel 'channel'
/*****************************************/
err_type TM_bimcrR(int channel, int *value)
/*****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->bim1cr[channel - 1].data;
  if (hwtype == PCI)
   *value = pcorbo->chan[channel - 1].irqcsr & 0xff; 

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(byte !!) into BIM2 CR of channel 'channel'
/*****************************************/
err_type TM_bim2crW(int channel, int value)
/*****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_bim2crW: writing value " << HEX(value));
    vcorbo->bim2cr[channel - 1].data = value;
  }
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].irqcsr = (u_int)value & 0xff;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(byte) from BIM2 CR register  of channel 'channel'
/******************************************/
err_type TM_bim2crR(int channel, int *value)
/******************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->bim2cr[channel - 1].data;
  if (hwtype == PCI)
   *value = pcorbo->chan[channel - 1].irqcsr & 0xff; 

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(byte !!) into BIM1 Vector register of channel 'channel'
/****************************************/
err_type TM_bimvrW(int channel, int value)
/****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_bimvrW: writing value " << HEX(value));
    vcorbo->bim1vr[channel - 1].data = value;
  }
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(byte) from BIM1 vector register  of channel 'channel'
/*****************************************/
err_type TM_bimvrR(int channel, int *value)
/*****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->bim1vr[channel - 1].data;
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


/*****************************************/
err_type TM_bim2vrW(int channel, int value)
/*****************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
  {
    DEBUG_TEXT(DFDB_RCCCORBO, 15, "TM_bim2vrW: writing value " << HEX(value));
    vcorbo->bim2vr[channel - 1].data = value;
  }
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


/******************************************/
err_type TM_bim2vrR(int channel, int *value)
/******************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->bim2vr[channel - 1].data;
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write  into test register of channel 'channel'
/***************************/
err_type TM_test(int channel)
/***************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    vcorbo->test[channel - 1] = 0xff;
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].test = 0xff;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write  into clear busy register of channel 'channel'
/****************************/
err_type TM_clbsy(int channel)
/****************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    vcorbo->clear[channel - 1] = 0xff;
  if (hwtype == PCI)
    pcorbo->chan[channel - 1].clear = 0xff;

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// write 'value'(16 bit) into RAM at 'offset'
/*************************************/
err_type TM_ramW(int offset, int value)
/*************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    vcorbo->ram[offset / 2] = value;
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


// read 'value'(16 bit) from RAM at 'offset'
/**************************************/
err_type TM_ramR(int offset, int *value)
/**************************************/
{
  OPEN_CHECK
  if (hwtype == VME)
    *value = vcorbo->ram[offset / 2];
  if (hwtype == PCI)
    return(RCC_ERROR_RETURN(0, CBE_NOSUPP));

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


//  perform special PCI-CORBO related actions
/*****************************************************/
err_type TM_etc(int channel, int function, int *iodata)
/*****************************************************/
{
volatile u_int data;

OPEN_CHECK
  
  if (function == 1)
  {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //print the IRQPEND register
    printf("IRQPEND=0x%08x\n", pcorbo->irqpend);
  }

  if (function == 2)
  {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //Enable interrupts
    plx->intcsr |= 0x00000900;
    printf("Interrupts enabled: PLX intcsr is 0x%08x\n", plx->intcsr);
  }

  if (function == 3)
  {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //Disable interrupts
    plx->intcsr &= 0xfffff6ff;
    printf("Interrupts disabled: PLX intcsr is 0x%08x\n", plx->intcsr);
  }

  if (function == 4)
  {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //Dump interrupt register
    data = plx->intcsr;
    printf("PLX intcsr is 0x%08x\n", data);
    printf("Interrupts are globally:   %s\n", (data & 0x00000100)?"Enabled":"Disabled");
    printf("Corbo interrupt is:        %s\n", (data & 0x00000800)?"Enabled":"Disabled");
    printf("Status of Corbo interrupt: %s\n", (data & 0x00008000)?"Active":"Not active");
  }

  if(function == 5)
   {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //IRQ test
    pcorbo->chan[channel - 1].csr = 0x2c;
    pcorbo->chan[channel - 1].irqcsr = 0x50;  
    data = pcorbo->chan[channel - 1].csr;
    data = pcorbo->chan[channel - 1].irqcsr;
    data = plx->intcsr;
    plx->intcsr = data | 0x00000900;
    data = plx->intcsr;
    printf("Corbo and PLX registers initialised.\n");
    printf("You can now trigger an interrupt.\n");
   }

  if (function == 6)
  {
    if (hwtype == VME)
      return(RCC_ERROR_RETURN(0, CBE_NOSUPP));
    //Set trigger and BUSY polarity
    DEBUG_TEXT(DFDB_RCCCORBO, 20, "TM_etc: iodata = " << HEX(*iodata));
    pcorbo->polarity = *iodata; 
  }

  if (function == 7)
  {
    if (hwtype == VME)
    {
      *iodata = hwtype;
      DEBUG_TEXT(DFDB_RCCCORBO, 10, "TM_etc: VMEbus CORBO detected");
    }
    if (hwtype == PCI)
    {
      *iodata = hwtype;
      DEBUG_TEXT(DFDB_RCCCORBO, 20, "TM_etc: PCI CORBO detected");
    }
  }

  return(RCC_ERROR_RETURN(0, CBE_OK));
}


/**********************************************************/
err_type cb_err_get(err_pack err, err_str pid, err_str code)
/**********************************************************/
{
  strcpy(pid, P_ID_CORBO_STR);

  switch(RCC_ERROR_MINOR(err))
  {
    case CBE_OK:          strcpy(code, CBE_OK_STR); break;
    case CBE_ISOPEN:      strcpy(code, CBE_ISOPEN_STR); break;
    case CBE_ISNOTOPEN:   strcpy(code, CBE_ISNOTOPEN_STR); break;
    case CBE_VMEOPEN:     strcpy(code, CBE_VMEOPEN_STR); break;
    case CBE_VMEMSTMAP:   strcpy(code, CBE_VMEMSTMAP_STR); break;
    case CBE_VMESLVMAP:   strcpy(code, CBE_VMESLVMAP_STR); break;
    case CBE_VMECLOSE:    strcpy(code, CBE_VMECLOSE_STR); break;
    case CBE_IORCC:       strcpy(code, CBE_IORCC_STR); break;
    case CBE_NOSUPP:      strcpy(code, CBE_NOSUPP_STR); break;
    default:              strcpy(code, CBE_NO_CODE_STR);
                          return(RCC_ERROR_RETURN(0, CBE_NO_CODE)); break;
  }

  return(RCC_ERROR_RETURN(0, CBE_OK));
}

