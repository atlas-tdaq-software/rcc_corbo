/****************************************************************/
/* file: corboscope.c                                           */
/*                                                              */
/* Author: Markus Joos, EP/ESS                                  */
/*                                                              */
/* 12. Dez. 00 MAJO created                                     */
/* 09. Nov. 01 JOP  adapt to RCC VMEbus API                     */
/* 06. Jun. 02 JOP  adapt to RCC IR API                         */
/*                                                              */
/*******C 2010 - A nickel program worth a dime*******************/

#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include "rcc_corbo.h"
#include "rcc_error/rcc_error.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/vme_rcc.h"

//prototypes
int clearc(int chan);
int clearcnt(int chan);
int setcnt(int chan);
int dumpc(int chan);
int dumpac(void);
int trigc(int chan);
int selectc(int *chan);
int initc(int chan);
int mainhelp(void);
int irqtest(int cochan);
int inloop(int cochan);
int wait_ts(int chan, int linkid);
int psemul(void);
void alarm_handler(int signum);
void SigQuitHandler(int signum);
void corbo_signal_handler(int signum);

//for the time stamping
enum
  {
  F1_START=1,
  F1_WAIT,
  F1_END
  };

//constants
#define COSIG  44 /*signal number for interrupt link*/
#define SOBSIG 45 /*signal number for interrupt link*/
#define EOBSIG 46 /*signal number for interrupt link*/
#define VME    0
#define PCI    1

//globals
static int inburst = 0, counter = 0, cont = 0;
static struct sigaction sa, sa2, sa3, sa4;
static sigset_t corbo_mask;
static sigset_t empty_mask;


// gets signal via driver - kill 
/***********************************/
void corbo_signal_handler(int signum)
/***********************************/
{
  if (signum == COSIG)  
  {
    printf("Signal %d received\n", signum);
    //TM_csrW(CHAN_ONE, DIS_CSR1);             // disable (int) busy eqv trigger i/p 
    //TM_clbsy(CHAN_ONE);                     // clear pending i/r 
    //TM_bimcrW(CHAN_ONE, INI_BIMCR0);         // reenable IE (BIM) 
    //TM_csrW(CHAN_ONE, INI_CSR1);             // (re)enable triggers 
  }
  else
    printf("unexpected signal\n");

}


// gets signal via driver - kill
/***********************/
void corbo_ps(int signum)
/***********************/
{
  if (signum == SOBSIG) 
  {
    printf("Start of burst signal received\n");
    TM_csrW(2, 0x6d);           // disable (int) busy eqv trigger i/p 
    TM_clbsy(2);               // clear pending i/r 
    TM_bimcrW(2, 0x5b);         // reenable IE (BIM) 
    TM_csrW(2, 0x6c);           // (re)enable triggers 
    inburst = 1;
  }
  else if(signum == EOBSIG)
  {
    printf("End of burst signal received\n");
    TM_csrW(3, 0x6d);           // disable (int) busy eqv trigger i/p 
    TM_clbsy(3);               // clear pending i/r 
    TM_bimcrW(3, 0x5b);         // reenable IE (BIM) 
    TM_csrW(3, 0x6c);           // (re)enable triggers 
    inburst = 0;
  }
  else
    printf("unexpected signal\n");
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int stat, doinit, sdata, cochan = 1, fun = 3, loop;
  u_int ret, io_base_add;

  if ((argc == 2)&&(sscanf(argv[1], "%d", &doinit) == 1)) {argc--;} else {doinit = 1;}
  if (argc != 1)
  {
    printf("use: %s [<initialize>]\n", argv[0]);
    printf("     initialize = 1 -> Initialize CORBO registers with defaults\n");
    printf("     initialize = 0 -> Do not touch any CORBO register\n");
    exit(0);
  }

  sigemptyset(&sa3.sa_mask);
  sa3.sa_flags = 0;
  sa3.sa_handler = corbo_signal_handler;
  if (sigaction(COSIG, &sa3, NULL) < 0 ) 
  {
    perror(" sigaction COSIG ");
    exit(1);
  }

  sigemptyset(&sa4.sa_mask);
  sa4.sa_flags = 0;
  sa4.sa_handler = corbo_ps;
  if (sigaction(SOBSIG, &sa4, NULL) < 0 )
  {
    perror(" sigaction SOBSIG ");
    exit(1);
  }
  if (sigaction(EOBSIG, &sa4, NULL) < 0 )
  {
    perror(" sigaction EOBSIG ");
    exit(1);
  }

  // block CORBO signals everywhere EXCEPT in sigsuspend
  sigemptyset(&corbo_mask);
  sigaddset(&corbo_mask, COSIG);
  //sigaddset(&corbo_mask, SOBSIG);
  //sigaddset(&corbo_mask, EOBSIG);
  sigprocmask(SIG_BLOCK, &corbo_mask, NULL);

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;

  stat = sigaction(SIGQUIT, &sa, NULL);
  if(stat <  0)
  {
    printf("Cannot install signal handler (error = %d)\n", stat);
    exit(1);
  }

  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = alarm_handler;
  if(sigaction(SIGALRM, &sa2, NULL)<0)
  {
    perror("sigaction SIGALRM\n");
    exit(1);
  }

  io_base_add = 0x700000;
  ret = TM_open(io_base_add);
  if(ret)
    rcc_error_print(stdout, ret);
 
  //disable all channels and interrupts; clear counters
  if(doinit)
  {
    for(loop = 1; loop < 5; loop++)
    {
      TM_csrR(loop, &sdata); sdata |= 0x1; TM_csrW(loop, sdata);
      TM_bimcrW(loop, 0); 
      TM_bimvrW(loop, 0);
      TM_cntW(loop, 0);
    }
  }

  fun = 1;
  printf("\n\n\nThis is CORBOSCOPE for VMEbus and a RCC\n");
  printf("=======================================\n");

  while(fun != 0)
  {
    printf("\n");
    printf("Active channel: %d\n", cochan);
    printf("Select an option:\n");
    printf("   1 Help                        2 Select channel\n");
    printf("   3 Initialise channel          4 Dump channel\n");
    printf("   5 Set counter                 6 Clear counter\n");
    printf("   7 Trigger channel (by S/W)    8 Clear channel\n");
    printf("   9 VME_RCC based IRQ test     10 Loop on input\n");
    printf("  11 Dump all channels          12 Check PS emulation\n");
    printf("   0 Quit\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) selectc(&cochan);
    if (fun == 3) initc(cochan);
    if (fun == 4) dumpc(cochan);
    if (fun == 5) setcnt(cochan);
    if (fun == 6) clearcnt(cochan);
    if (fun == 7) trigc(cochan);
    if (fun == 8) clearc(cochan);
    if (fun == 9) irqtest(cochan);
    if (fun == 10) inloop(cochan);
    if (fun == 11) dumpac();
    if (fun == 12) psemul();
  }
  exit(0);
}  


/********************************/
void alarm_handler(int /*signum*/)
/********************************/
{
  printf("Counter is: %d\n", counter);
  if (cont)
    alarm(2);
}


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont = 0;
  alarm(0);
}


/**************/
int psemul(void)
/**************/
{
  u_char ivect1 = 0, ivect2 = 0;
  int int_level, data, nol, linkid1, linkid2;
  VME_InterruptList_t irq_list;
  u_int ret;

  printf("Enter the IRQ vector for the start of burst (Corbo channel 2) ");
  ivect1 = gethexd((unsigned int)ivect1);
  printf("Enter the IRQ vector for the end of burst (Corbo channel 3) ");
  ivect2 = gethexd((unsigned int)ivect2);
  printf("Enter the IRQ level ");
  int_level = getdecd(3);

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = ivect1;
  irq_list.list_of_items[0].level = int_level;
  irq_list.list_of_items[0].type = VME_INT_ROAK;

  ret = VME_InterruptLink(&irq_list, &linkid1);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = ivect2;
  irq_list.list_of_items[0].level = int_level;
  irq_list.list_of_items[0].type = VME_INT_ROAK;

  ret = VME_InterruptLink(&irq_list, &linkid2);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("Press ctrl+\\ to exit from loop\n");
  cont = 1;

  nol = 10;
  while (cont)
  {
    //sigsuspend(&empty_mask);
    if(inburst)
    {
      TM_cntR(1, &data);
      if (nol)
        {
        printf("Trigger counter is at %d\n", data);
        nol--;
        }
      }
    else
      nol = 10;
    }

  ret = VME_InterruptUnlink(linkid1);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_InterruptUnlink(linkid2);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return(0);
}
 
 
/*********************/
int irqtest(int cochan)
/*********************/
{
  int timeout = 10000;
  u_char ivect = 0;
  int int_level;
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;

  int sig = COSIG, linkid[4];
  int fun;
  unsigned int ret = 0;

  fun = 1;
  printf("=================================\n");

  while (fun != 0)
  {
    printf("\n");
    printf("Active channel: %d\n", cochan);
    printf("Select an option:\n");
    printf("   1 Open                        2 Link\n");
    printf("   3 Wait                        4 Unlink\n");
    printf("   5 Print                       6 Close\n");
    printf("   7 Wait with time stamping\n");
    printf("   0 Quit\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) 
    {
      ret = VME_Open();
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    if (fun == 2) 
    {
      printf("Enter the VMEbus interrupt vector\n");
      ivect = gethexd((unsigned int)ivect);
      printf("Enter the IRQ level ");
      int_level = getdecd(3);
    
      irq_list.number_of_items = 1;
      irq_list.list_of_items[0].vector = ivect;
      irq_list.list_of_items[0].level = int_level;
      irq_list.list_of_items[0].type = VME_INT_ROAK;

      ret = VME_InterruptLink(&irq_list, &linkid[cochan - 1]);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    if (fun == 3) 
    {
      if(sig) //signal
        sigsuspend(&empty_mask);   // wait for any signal 
      else    //semaphore
        ret = VME_InterruptWait(linkid[cochan-1], timeout, &ir_info);
   
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    if (fun == 4)
    {
      ret = VME_InterruptUnlink(linkid[cochan - 1]);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    if (fun == 5)
    {
      printf("Not implemented\n");
    }
    if (fun == 6)
    {
      ret = VME_Close();
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    } 
    if (fun == 7)
    {
      ret = wait_ts(cochan, linkid[cochan - 1]);
    }
    rcc_error_print(stdout, ret);
    printf("=================================\n");
  }
  return(0);
}


/*******************************/
int wait_ts(int chan, int linkid)
/*******************************/
{
  static char fname[50];
  static int num = 1000;
  int loop, timeout = 10000;
  VME_ErrorCode_t ret;
  VME_InterruptInfo_t ir_info;

  printf("Enter number of triggers to process ");
  num = getdecd(num);
  printf("Enter name of output file ");
  getstrd(fname, (char *)"pci_corbo");

  ret = ts_open(num, TS_H1);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_start(TS_H1);
  if (ret)
    rcc_error_print(stdout, ret);

  TM_clbsy(chan);
  for(loop = 0; loop < num; loop++)
  {
    ret = VME_InterruptWait(linkid, timeout, &ir_info);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);

    TM_clbsy(chan);

  ret = ts_record(TS_H1, F1_WAIT);
  if (ret)
    rcc_error_print(stdout, ret);
  }

  ret = ts_save(TS_H1, fname);
  if (ret)
    rcc_error_print(stdout, ret);

  ret = ts_close(TS_H1);
  if (ret)
    rcc_error_print(stdout, ret);

  return(0);
}


/*****************/
int trigc(int chan)
/*****************/
{
  printf("\n=========================================================================\n");
  TM_test(chan);
  printf("BUSY of channel %d set.\n", chan);
  printf("=========================================================================\n\n");
  return(0);
}


/********************/
int selectc(int *chan)
/********************/
{
  printf("\n=========================================================================\n");
  do
  {
    printf("Enter the number of the channel to use from now on ");
    *chan = getdecd(*chan);
  }
  while(*chan < 1 || *chan > 4);
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int initc(int chan)
/*****************/
{
  int d1 = 1, d2 = 0, d3 = 1, d4 = 0, d5 = 0, d6 = 3, d7 = 0x50;
  u_short sdata;

  printf("\n=========================================================================\n");
  printf("Enable channel                      (1=yes  0=no) ");
  d1 = getdecd(d1);
  printf("Counter selection (1=event ID  0=missed triggers) ");
  d2 = getdecd(d2);
  printf("Fast clear enable                   (1=yes  0=no) ");
  d3 = getdecd(d3);
  printf("Input selection                     (0=NIM 3=S/W) ");
  d4 = getdecd(d4);
  d1 &= 0x1;
  d2 &= 0x1;
  d3 &= 0x1;
  if(d4) d4 = 3;

  sdata = (d4 << 2) | ((1 - d3) << 6) | (d2 << 5) | (1 - d1);  /*inverted logic for d1 and d3*/
  TM_csrW(chan, sdata);

  printf("Enter the interrupt level                         ");
  d6 = getdecd(d6);
  printf("Enable interrupt                    (1=yes  0=no) ");
  d5 = getdecd(d5);
  sdata = ((d5 & 0x1)<<4) | 0x40 | d6; /*set auto clear flag!*/
  TM_bimcrW(chan, sdata);


  printf("Enter the interrupt vector                        ");
  d7 = gethexd((u_int)d7);
  TM_bimvrW(chan, d7);
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int dumpc(int chan)
/*****************/
{
  int sdata, ldata;

  printf("\n=========================================================================\n");
  TM_csrR(chan, &sdata);
  sdata &= 0xfff;
  printf("TM_csrR reads 0x%04x\n", sdata);
  printf("Channel is:                     %s\n", sdata&0x1?"Disabled":"Enabled");
  printf("BUSY:                           %s\n", sdata&0x2?"Follows the input":"Is a level set by the input");
  printf("Selected input:                 ");
  if((sdata & 0xc) == 0x0) printf("Front panel\n");
  else if ((sdata & 0xc) == 0x4) printf("Differential input bus\n");
  else if ((sdata & 0xc) == 0x8) printf("Differential output bus\n");
  else if ((sdata & 0xc) == 0xc) printf("Internal test pulse\n");
  printf("BUSY output selection:          %s\n", sdata & 0x10?"Differential output BUSY":"Local BUSY");
  printf("Counter selection:              %s\n", sdata & 0x20?"Count BUSY (=event ID)":"Count input");
  printf("Fast clear is:                  %s\n", sdata & 0x40?"Disabled":"Enabled");
  printf("Push button is:                 %s\n", sdata&0x80?"Disabled (resets IRQ)":"Enabled");
  printf("Input state:                    %s\n", sdata&0x100?"Input present":"No input present");
  printf("Local BUSY state:               %s\n", sdata & 0x200?"Local BUSY present":"No local BUSY present");
  printf("Differential output BUSY state: %s\n", sdata & 0x400?"Differential BUSY present":"No differential BUSY present");
  printf("Interrupt state:                %s\n", sdata & 0x800?"No IRQ pending":"IRQ pending");

  TM_cntR(chan, &ldata);
  printf("\nValue of counter:               %d (0x%08x)\n", ldata, ldata);

  TM_bimcrR(chan, &sdata);
  printf("\nTM_bimcrR reads 0x%04x\n", sdata);
  printf("Interrupt level:                %d\n", sdata & 0x7);
  printf("Auto clear interrupt enable:    %s\n", sdata & 0x8?"Yes":"No");
  printf("Interrupt enabled:              %s\n", sdata & 0x10?"Yes":"No");
  printf("Auto Clear flag:                %s\n", sdata & 0x40?"Set":"Not set");

  TM_bimvrR(chan, &sdata);
  printf("\nTM_bimvrR reads 0x%04x\n", sdata);
  printf("Interrupt vector:               0x%02x\n", sdata);
  printf("=========================================================================\n\n");
  return(0);
}


/**************/
int dumpac(void)
/**************/
{
  int loop, sdata, idata, cdata;

  printf("\n=====================================================================================================================\n");
  printf("Channel |   CSR |  BIM | enabled | input via | input | busy | IRQ | AC-flag | interrupt | level | vector |    counter\n");

  for(loop = 1; loop < 5; loop++)
  {
    TM_csrR(loop, &sdata);
    TM_cntR(loop, &cdata);
    TM_bimcrR(loop, &idata);
    sdata &= 0xfff;

    printf("      %d |", loop);
    printf(" 0x%03x |", sdata);
    printf(" 0x%02x |", idata);
    printf("     %s |", sdata & 0x1?" No":"Yes");
    if((sdata & 0xc) == 0x0) printf("      Lemo |");
    else if((sdata & 0xc) == 0x4) printf("   Diff in |");
    else if((sdata & 0xc) == 0x8) printf("  Diff out |");
    else if((sdata & 0xc) == 0xc) printf("       S/W |");
    printf("   %s |", sdata & 0x100?"Yes":" No");
    printf("  %s |", sdata & 0x200?"Yes":" No");
    printf(" %s |", sdata & 0x800?" No":"Yes");
    printf("     %s |", idata & 0x040?"Yes":" No");
    printf("  %s |", idata & 0x010?" enabled":"disabled");
    printf("     %d |", idata & 0x7);
    TM_bimvrR(loop, &idata);
    printf("   0x%02x |", idata);
    printf(" 0x%08x \n", cdata);
  }

  printf("=====================================================================================================================\n\n");
  return(0);
}


/******************/
int clearc(int chan)
/******************/
{
  printf("\n=========================================================================\n");
  TM_clbsy(chan);
  printf("Channel %d cleared\n", chan);
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int inloop(int chan)
/******************/
{
  char fname[50];
  int mode = 0, num = 1000;
  int loop, data;
  unsigned int ret;

  printf("\n=========================================================================\n");

  TM_cntW(chan, 0);
  data = 1;
  printf("Enter the mode  (0=run forever  1=time stamping) ");
  mode = getdecd(mode);

  if(mode == 1)
  {
    printf("Enter number of triggers to process ");
    num = getdecd(num);
    printf("Enter name of output file ");
    getstrd(fname, (char *)"pci_corbo");

    ret = ts_open(num, TS_H1);
    if (ret)
      rcc_error_print(stdout, ret);
    ret = ts_start(TS_H1);
    if (ret)
      rcc_error_print(stdout, ret);

    for (loop = 0;loop < num;loop++)
    { 
      do
        TM_cntR(chan, &counter);
      while (counter == data);
      data = counter;
      TM_clbsy(chan);

      ret = ts_record(TS_H1, F1_WAIT);
      if (ret)
        rcc_error_print(stdout, ret);
      }

    ret = ts_save(TS_H1, fname);
    if (ret)
      rcc_error_print(stdout, ret);
    ret = ts_close(TS_H1);
    if (ret)
      rcc_error_print(stdout, ret);
    }
    else
    {
      printf("Press ctrl+\\ to exit from loop\n");
      cont = 1;
      alarm(2);
      while (cont)
      {
        do
          TM_cntR(chan, &counter);
        while (counter == data);
        data = counter;
        TM_clbsy(chan);  
      }
    } 

  printf("=========================================================================\n\n");
  return(0);
}


/********************/
int clearcnt(int chan)
/********************/
{
  printf("\n=========================================================================\n");
  TM_cntW(chan, 0);
  printf("Counter of channel %d cleared\n", chan);
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int setcnt(int chan)
/******************/
{
  u_int num = 0xff;

  printf("\n=========================================================================\n");
  printf("Enter counter value ");
  num = gethexd(num);
  TM_cntW(chan, (int)num);
  printf("Counter of channel %d set to %d\n", chan, num);
  printf("=========================================================================\n\n");
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("That's all for now.\n");
  printf("=========================================================================\n\n");
  return(0);
}

