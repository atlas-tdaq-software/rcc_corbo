/* VME trigger module test program : vmetrig_test2.c 

   triggers via CORBO front panel NIM input channel2 

   using semaphores to synchronise : gpih_wait

NB! the VME interrupt level must be enabled eg. :  /bin/ces/vmeconfig

  The program also measures the number of interrupts handled per second.

  96/07/08  J.O.Petersen ECP

*/

#include <stdio.h>
#include <errno.h>
/*#include <file.h>*/
#include <time.h>
#include "gpihlib.h"
#include "vmetrig.h"

int getdec();
char getfstchar();

/* CORBO definitions */
#define CHAN_ONE 1
#define CHAN_TWO 2
#define CHAN_THREE 3
#define CHAN_FOUR 4

/* (internal) busy enabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define INI_CSR1 0xc0

/* (internal) busy disabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define DIS_CSR1 0xc1

/* interrupt enable (IRE), autoclear, level 3 */
#define INI_BIMCR0 0x1b

/* interrupt disable (level 0) */
#define DIS_BIMCR0 0x00

#define VME_VEC_NUM 131
#define SIGNUM 0 

main()
{

int pathnum,vecnum0,signum0,linkid0,istat;
int i,j;
unsigned int io_base_add=0x700000;

char ich = 'y';
time_t tbeg,tend;
int t;
int nloops = 100;               /* default # pulses */
                                                                                
  if( TM_open(io_base_add) != 0 ) {
    printf(" TM_Open error : vme base address defined ?\n");
    exit(0);
  }
                                                                                
/* clear from out to in, enable from in to out */
  TM_csrW(CHAN_TWO,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
  TM_clbsy(CHAN_TWO);		/* clear pending i/r */
  TM_bimcrW(CHAN_TWO,DIS_BIMCR0);

  TM_bimvrW(CHAN_TWO,VME_VEC_NUM);	/* VME vector */
  TM_bimcrW(CHAN_TWO,INI_BIMCR0);
/*  TM_csrW(CHAN_TWO,INI_CSR1);*/	/* enable triggers : after linking */

  istat = gpih_open("/dev/gpih",&pathnum);
  if(istat != 0) {
    printf(" gpih_open error %d errno = %d\n :",istat, errno);
    exit(0);
  }
  else
    printf(" Opened path number %4d\n",pathnum);

#ifdef DUMP
  istat = gpih_print(pathnum);
#endif

  vecnum0 = VME_VEC_NUM; signum0 = SIGNUM;	/* VME interrupts, NO signal */
  istat = gpih_link(pathnum,GPIH_VME,vecnum0,signum0,&linkid0);
  if(istat != 0) {
    printf(" gpih_link error %d errno = %d :\n",istat, errno);
    exit(0);
  }

  printf(" vmetrig : gpih_link on first link : status =%4d\n",istat);
  printf(" vmetrig : vect =%4d signum = %4d linkid = %d\n",
         vecnum0,signum0,linkid0);

#ifdef DUMP
  istat = gpih_print(pathnum);
#endif

  while (ich == 'y') {
    printf(" # loops = ");
    nloops = getdec();
    time(&tbeg);

    for (i=1;i<=nloops;i++) {
      TM_csrW(CHAN_TWO,INI_CSR1); 	/* (re)enable triggers */

/* exit on signal - the process may die before returning from gpih_wait !! */
      istat = gpih_wait(pathnum, linkid0, GPIH_WAIT_SIGABORT);
      if(istat != 0) {
        printf(" gpihwait error %d errno = %d :\n",istat, errno);
        if (istat == GPIH_WTABORT ) {
          printf(" wait aborted by signal \n");
        }
        exit(0);
      }

      TM_csrW(CHAN_TWO,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
      TM_clbsy(CHAN_TWO);		/* triggers may arrive, but disabled  */

      TM_bimcrW(CHAN_TWO,INI_BIMCR0); 	/* reenable IE (BIM) */
    } /* for */

    time(&tend);
    t = difftime(tend, tbeg);
    printf(" elapsed time : %.2lf secs \n", (double) t);
    if (t) printf(" # loops per second  : %d \n",
            nloops / t);

    printf(" restart  (y/n) : ");
    ich = getfstchar();

  } /* while*/

/*  clean everything */

/****************************************
  istat = gpih_unlink(pathnum,linkid0);
  if(istat != 0) {
    printf(" gpih_unlink error %d errno = %d :",istat, errno);
    exit(0);
  }
******************************************/

#ifdef DUMP
  istat = gpih_print(pathnum);
#endif

  istat = gpih_close(pathnum);
  if(istat != 0) {
    printf(" gpih_close error %d errno = %d :",istat, errno);
    exit(0);
  }
  else
    printf(" Closed path number %4d\n",pathnum);

#ifdef DUMP
  istat = gpih_print(pathnum);
#endif

  TM_close();
}

int getdec()  /*  get ONE decimal integer */

{
        char sbuf[10];   /* max 9 chars */
        int nfield,nint;

        do {
                fgets(sbuf,10,stdin);
                nfield = sscanf(sbuf,"%d",&nint);
                if (nfield<1) printf(" ??? : ");
           } while (nfield<1);

        return(nint);
}

char getfstchar()  /*  get first character */
		   /* simpler with int !! */	

{
	char sbuf[10];   /* max 9 chars */

		fgets(sbuf,10,stdin);

	return(sbuf[0]);
}
