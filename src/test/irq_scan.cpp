#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "DFDebug/DFDebug.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/vme_rcc.h"
#include "rcc_corbo.h"

// CORBO definitions
#define CHAN_ONE       1
#define INI_CSR1       0xcc  // (internal) channel enabled, busy latch, internal input, internal busy out, count triggers, disable external counter clear, pushbutton resets BIM
#define DIS_CSR1       0xcd  // (internal) channel disabled, busy latch, internal input, internal busy out, count triggers, disable external counter clear, pushbutton resets BIM
#define INI_BIMCR0     0x18  // interrupt enable (IRE), autoclear, level 0
#define DIS_BIMCR0     0x00  // interrupt disable (level 0)
#define SIGNUM         42    // Qed signal

static struct sigaction sa;
static int waiting;
static int int_handle;


/***********************************/
void corbo_signal_handler(int signum)
/***********************************/
{
  VME_ErrorCode_t ret;
  
  if (signum == SIGNUM) 
  { 
    VME_InterruptInfo_t intrpt_info;
    ret = VME_InterruptInfoGet(int_handle, &intrpt_info);

    if(ret == VME_NOINTERRUPT)
    {
      printf("ERROR: VME_NOINTERRUPT received from VME_InterruptInfoGet\n");
      return;
    }
  
    if(ret)
    {
      VME_ErrorPrint(ret);
      return;
    }

    printf( "VMEbus interrupt seen: level=%d, vector=%d, mult=%d\n", intrpt_info.level, intrpt_info.vector, intrpt_info.multiple);
    waiting = 0;
  }  
  else
  { 
    printf("unexpected signal\n");
    exit(-1);
  }  
}



/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int mode, irq_level, irq_vector;
  u_int io_base_add = 0x700000;
  VME_ErrorCode_t ret;
  VME_InterruptList_t vmebus_vector_list;       
  VME_InterruptList_t intrpt_list[8];
  tstamp ts1, ts2;
  float delta;
  
  printf("argc = %d\n", argc);
  mode = 1;

  if ((argc == 2)&&(sscanf(argv[1], "%d", &mode) == 1)) {argc--;} else {mode = 1;}
  if (argc != 1)
  {
    printf("use: %s [<mode>]\n", argv[0]);
    printf("     mode = 1 -> Full IRQ vector and level scan\n");
    printf("     mode = 2 -> IRQ scan a la Henk (whatever that is)\n");
    printf("     mode = 3 -> time-out check\n\n");
    exit(0);
  }

  DF::GlobalDebugSettings::setup(20, 102);

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = corbo_signal_handler;
  // dont block any signal in intercept handler
  if (sigaction(SIGNUM, &sa, NULL) < 0) 
  {
    perror(" sigaction GL_Signal ");
    exit(1);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = TM_open(io_base_add);
  if( ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Open OK\n");
      
  if(mode == 1)
  {
    printf("Test 1: Check all levels and vectors with signals\n");
    for (irq_level = 1; irq_level < 8; irq_level++)
      for (irq_vector = 0; irq_vector < 256; irq_vector++)
      {
	// clear from out to in, enable from in to out
	TM_csrW(CHAN_ONE, DIS_CSR1);	                // disable (int) busy eqv trigger i/p 
	TM_clbsy(CHAN_ONE);		                // clear pending i/r
	TM_bimcrW(CHAN_ONE, DIS_BIMCR0);
	TM_bimvrW(CHAN_ONE, irq_vector);	
	TM_bimcrW(CHAN_ONE, INI_BIMCR0 + irq_level);

	vmebus_vector_list.number_of_items = 1;
	vmebus_vector_list.list_of_items[0].vector = irq_vector;
	vmebus_vector_list.list_of_items[0].level = irq_level;
	vmebus_vector_list.list_of_items[0].type = VME_INT_ROAK;

	ret = VME_InterruptLink(&vmebus_vector_list, &int_handle);
	if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

	ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
	if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

	TM_csrW(CHAN_ONE, INI_CSR1); 	// (re)enable triggers
	printf("Waiting for interrupt with level=%d and vector=%d\n", irq_level, irq_vector);

	waiting = 1;
	TM_test(CHAN_ONE);

	while(waiting)
	  sleep(1);
	
	printf("waited\n");

	ret = VME_InterruptRegisterSignal(int_handle, 0);
	if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);     

	ret = VME_InterruptUnlink(int_handle);
	if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);     
      }
  }  
  
  if (mode == 2)
  {
    printf("\n\n\n\nTest 2: Check all levels and some vectors a la Henk\n");
    for(irq_level = 1; irq_level < 8; ++irq_level)
    {
      for(irq_vector = 0; irq_vector < 16; ++irq_vector)
      {
	intrpt_list[irq_level].list_of_items[irq_vector].level  = irq_level;
	intrpt_list[irq_level].list_of_items[irq_vector].vector = irq_vector;
	intrpt_list[irq_level].list_of_items[irq_vector].type   = VME_INT_ROAK;
      }
      intrpt_list[irq_level].number_of_items = 16;
    }

    for(irq_level = 1; irq_level < 2; ++irq_level)
    {
      ret = VME_InterruptLink(&intrpt_list[irq_level], &int_handle);
      if(ret)
	VME_ErrorPrint(ret);

      ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
      if(ret)
	VME_ErrorPrint(ret);

      for(irq_vector = 0; irq_vector < 16; ++irq_vector)
      {
	TM_csrW(CHAN_ONE, DIS_CSR1);	               
	TM_clbsy(CHAN_ONE);		    
	TM_bimcrW(CHAN_ONE, DIS_BIMCR0);
	TM_bimvrW(CHAN_ONE, irq_vector);	
	TM_bimcrW(CHAN_ONE, INI_BIMCR0 + irq_level);
	TM_csrW(CHAN_ONE, INI_CSR1); 	
	printf("Waiting for interrupt with level=%d and vector=%d\n", irq_level, irq_vector);

	waiting = 1;
	TM_test(CHAN_ONE);

	while(waiting){};
      }

      ret = VME_InterruptRegisterSignal(int_handle, 0);
      if(ret)
	VME_ErrorPrint(ret);

      ret = VME_InterruptUnlink(int_handle);
      if(ret)
	VME_ErrorPrint(ret);
    }
  }
  
  
  if (mode == 3)
  {
    printf("\n\n\n\nTest 3: Check timeout\n");
    vmebus_vector_list.number_of_items = 1;
    vmebus_vector_list.list_of_items[0].vector = 100;
    vmebus_vector_list.list_of_items[0].level = 3;
    vmebus_vector_list.list_of_items[0].type = VME_INT_ROAK;

    ret = ts_open(1, TS_DUMMY);
    if (ret)
      rcc_error_print(stdout, ret);

    ret = VME_InterruptLink(&vmebus_vector_list, &int_handle);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);

    ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);

    VME_InterruptInfo_t ir_info;

    ts_clock(&ts1);
    ret = VME_InterruptWait(int_handle, 10000, &ir_info);
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    printf(" 10 seconds requested. Measured: %6.2f seconds\n", delta);
    exit(0);

    ret = VME_InterruptRegisterSignal(int_handle, 0);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);     

    ret = VME_InterruptUnlink(int_handle);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);     

    ret = ts_close(TS_DUMMY);
    if (ret)
      rcc_error_print(stdout, ret);
  }
    
  //  clean everything
  ret = TM_close();
  if(ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Close OK\n");

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return 1;
}


