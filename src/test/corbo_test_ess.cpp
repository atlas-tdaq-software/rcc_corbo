#include        <stdio.h> 
#include 	<errno.h>
#include 	<stdlib.h>
#include 	<signal.h>
#include        <unistd.h>
#include        "rcc_corbo.h"
#include 	"vme_rcc/vme_rcc.h"
#include        "vme_rcc/universe.h"


/*still to solve.....     					*/
/*description of the subtests in menu point e,E			*/
/*differential signals						*/
/*interrupt on busy overflow					*/
/*test 5:Is the TTL signal active low or active high?		*/

/****************************************************************/
/*								*/
/*	This is the official ESS-OS test program for the	*/
/*			CES RCB 8047 CORBO			*/
/*								*/
/*								*/
/*	Author: Markus Joos, ECP/ESS/OS				*/
/*								*/
/*	History:						*/
/*	26.Oct.94  MAJO  created				*/
/*	 1.Dec.94  BLTH  translated into french			*/
/*	 2.Dec.94  MAJO  last update				*/
/*								*/
/****************************************************************/

/*Prototypes*/
void icpt(int signal);
void menu(void);
void help(void);
void items(void);
void inst(void);
int start_test(void);
int single(void);
int init_corbo(void);
void close_corbo(void);
int test1(void);
int test2(void);
int test3(void);
int test4(void);
int test5(void);
int test6(void);
int test7(void);
int test8(void);
int test9(void);
int test10(void);
int test11(void);
int test12(void);
void rfc(unsigned int address, unsigned short *data);
void wtc(unsigned int address, unsigned short data);
void ir_on(int level);
void ir_off(int level);
void ex(int num);
void ir_warning(void); 
int ir_test(int level);
void prtc(void);


/*globals*/
int key,siggi, handle;
char dummy[20];
int lasttest,verbose=0;
int npending,pathnum,linkid1,linkid2;
unsigned int *des_ptr, *address, data, stat, cbase;
unsigned long unibase;
static VME_MasterMap_t master_map = {0x0, 0x1000000, VME_A24, 0};
struct sigaction sa;
universe_regs_t *uni;

/*constants*/
#define VME_INTTIMEOUT 1

#define VMETR_CSR1      0X00    /* status registers     */
#define VMETR_CSR2      0X02
#define VMETR_CSR3      0X04
#define VMETR_CSR4      0X06

#define VMETR_CNT1      0X10    /* counter              */
#define VMETR_CNT2      0X14
#define VMETR_CNT3      0X18
#define VMETR_CNT4      0X1C

#define VMETR_TOU1      0X20    /* time out             */
#define VMETR_TOU2      0X22
#define VMETR_TOU3      0X24
#define VMETR_TOU4      0X26

#define VMETR_BIM1      0X30    /* BIM                  */
#define VMETR_BIM2      0X40
#define OFST_CR0        0X01    /*      control         */
#define OFST_CR1        0X03
#define OFST_CR2        0X05
#define OFST_CR3        0X07
#define OFST_VR0        0X09    /*      vector          */
#define OFST_VR1        0X0B
#define OFST_VR2        0X0D
#define OFST_VR3        0X0F

#define VMETR_TEST1     0X50    /* test (to generate)   */
#define VMETR_TEST2     0X52
#define VMETR_TEST3     0X54
#define VMETR_TEST4     0X56

#define VMETR_CLEAR1    0X58    /* clear                */
#define VMETR_CLEAR2    0X5A
#define VMETR_CLEAR3    0X5C
#define VMETR_CLEAR4    0X5E

#define VMETR_RAM       0X60    /* RAM                  */

#define VMETR_AM        0x39
#define VMETR_LEN       0x100


/*************/
void menu(void)		/*print the main menu*/
/*************/
{
int active=1;
char mopt[1];

do
  {
  printf("Ce programme permet de tester le module RCB 8047 CORBO\n");
  printf("MENU\n");
  printf(" A - Aide en ligne\n");
  printf(" L - Liste du materiels necessaires\n");
  printf(" I - Information concernant l'installation des tests\n");
  printf(" D - Debut des tests\n");
  printf(" E - Execution d'un seul test\n");
  printf(" F - Fin\n");
  printf("Votre choix: ");
  fgets(mopt, 1, stdin);
  if (mopt[0]==69 || mopt[0]==101) single();		/*E,e*/
  if (mopt[0]==65 || mopt[0]==97) help();		/*A,a*/
  if (mopt[0]==76 || mopt[0]==108) items();		/*L,l*/
  if (mopt[0]==73 || mopt[0]==105) inst();		/*I,i*/
  if (mopt[0]==67 || mopt[0]==99) verbose=1;		/*C,c*/
  if (mopt[0]==80 || mopt[0]==112) verbose=0;		/*P,p*/
  if (mopt[0]==68 || mopt[0]==100) start_test();	/*D,d*/
  if (mopt[0]==70 || mopt[0]==102) ex(0); 		/*F,f*/
  }
while(active);
}


/*************/
void help(void)	/*print help information*/
/*************/
{
printf("Ce programme permet de tester le module RCB 8047 CORBO\n");
printf(" \n");
printf("Voici la signification du menu\n");
printf(" \n");
printf(" \n");
printf("A - Aide en ligne\n");
printf("    Affichage de ce texte\n");
printf(" \n");
printf("L - Liste du materiels necessaires\n");
printf("    Affiche une liste complete de tous les elements\n");
printf("    (modules VME, cables, outils, documents, etc)\n");
printf("    necessaires pour l'execution des tests. Il est important\n");
printf("    de reunir tous ces elements avant de commencer.\n");
printf(" \n");
printf("I - Information concernant l'installation\n");
printf("    Explique etape par etape comment installer le module a\n");
printf("    tester en utilisant les elements mentionnnes par (L).\n");
printf(" \n");
printf("D - Debut des tests\n");
printf("    Lance le debut des routines de tests. Ne pas utiliser\n");
printf("    cette option avant l'instalation complete du systeme.\n");
printf(" \n");
printf("F - Fin\n");
printf("    Termine le programme de test\n");
printf(" \n");
printf("E - Execution d'un seul test\n");
printf("    Si une des routines de test ne marche pas il n'est pas\n");
printf("    necessaire de refaire tous les tests. Utiliser plutot\n");
printf("    cette fonction pour repasser les tests qui ont echoues\n");
printf(" \n");
printf(" \n");
printf(" Pour tous renseignements concernant la preparation ou l'execution de ces programmes de tests contacter une des personnes suivantes :\n");
printf(" \n");
printf("Markus Joos      Tel.:72364\n");
printf("Catherine Moine  Tel.:75035\n");
}


/**************/
void items(void)		/*print list of items needed for the test*/
/**************/
{
printf(" Liste du materiels necessaires pour les tests\n");
printf(" \n");
printf("Les elements entre crochets font parti du 'kit de test du CORBO'\n");
printf(" \n");
printf(" 1 coffret VME (rack, crate merci Toubon) \n");
printf(" 1 module CES FIC 8234 CPU\n");
printf(" 1 generateur d'impulsions HAMEG (Model HM 8035)\n");
printf(" 1 oscilloscope (Model XXXX)\n");
printf(" 1 convertisseur NIM vers ECL\n");
printf(" 1 terminal Falco\n");
printf(" 1 cable RS232 (pour connecter le FIC au terminal)\n");
printf("[2 cables LEMO (2 m)]\n");
printf("[3 adapteurs LEMO vers BNC]\n");
printf("[1 cable 8-lignes avec 2 connecteurs 3M]\n");
printf("[1 cable 8-lignes avec 1 connecteur 3M]\n");
printf(" 1 Manual 'ESS-OS RCB8047 CORBO test program'\n");
printf(" 1 Check list\n");
}


/*************/
void inst(void)		/*print installation information*/
/*************/
{
printf("Information concernant l'installation\n");
printf(" \n");
printf(" \n");
printf(" \n");
printf("Notes:\n");
printf(" \n");
printf("* Les references concernant les chapitres cites ci-dessous\n");
printf("  se trouvent dans le document 'ESS-OS RCB8047 CORBO test program'.\n");
printf("* Les modules VME utilises lors des tests sont sensibles aux\n");
printf("  decharges electrostatique. Les placer uniquement sur des\n"); 
printf("  surfaces anti-statiques. Et ne pas les manipuler sans utiliser\n");
printf("  un bracelet anti-statique.\n");
printf(" \n");
printf(" \n");
printf("Etape  Action\n");
printf(" \n");
printf("1.     Fill in the check-list \n");
printf(" \n");
printf("2.      Preparation du coffret VME\n");
printf("2.1    Eteindre le coffret\n");
printf("2.2    Retirer toutes les cartes\n");
printf(" \n");
printf("3.      Installation du FIC8234\n");
printf("3.1    Verifier la presence du pont J6 (voir fig. 1).\n");
printf("3.2    Inserer le FIC8234 dans le slot 1 du coffret VME\n");
printf("3.3    Connecter le cable Ethernet au connecteur BNC du FIC8234\n");
printf("3.4    Connecter le port 'terminal' du FIC8234 avec le\n");
printf("       terminal Falco\n");
printf(" \n");
printf("4.      Installation du CORBO a tester\n");
printf("4.1    Remplir la section 'configuration' de la check-list\n");
printf("       en se referant a la configuration de la carte.\n");
printf("4.2    Placer les 10 ponts du panneau avant sur 'N' (voir fig. 2\n");
printf("4.3    Set the base address to Hex. 7000 (see fig. 3)\n");
printf("4.3    Inserer la carte dans le slot 4\n");
printf(" \n");
printf("5.      Installation du convertisseur NIM vers ECL\n");
printf("       (.............)\n");
printf(" \n");
printf("6.      Preparation de l'oscilloscope\n");
printf("6.1  Equip the input of channel 1 with a BNC to LEMO converter\n");                                          
printf("6.2  Switch it on\n");
printf(" \n");
printf("7.      Preparation du generateur d'impulsions\n");
printf("7.1  Equip both output channels with  BNC to LEMO converters\n");
printf("7.2  Switch it on\n");
printf(" ");
printf("8.      Allumer le coffret VME\n");
printf(" \n");
printf("9.      Run the program 'corbo'\n");
}


/******************/
int start_test(void)  	/*start the test*/
/******************/
{
int first;

stat=init_corbo(); 
if (stat!=0)
  {
  printf("Error in init_corbo\n");
  menu();
  }

printf("Enter the number of the test (1..12) to start with\n"); 
scanf("%d", &first);
printf("\n");

if (first<2)
  {stat=test1(); if (stat!=0) menu();}
if (first<3)
  {stat=test2(); if (stat!=0) menu();}
if (first<4)
  {stat=test3(); if (stat!=0) menu();}
if (first<5)
  {stat=test4(); if (stat!=0) menu();}
if (first<6)
  {stat=test5(); if (stat!=0) menu();}
if (first<7)
  /*{stat=test6(); if (stat!=0) menu();}*/		/*time out IR*/
if (first<8)
  {stat=test7(); if (stat!=0) menu();}
if (first<9)
  /*{stat=test8(); if (stat!=0) menu();}*/		/*diff.*/
if (first<10)
  /*{stat=test10(); if (stat!=0) menu();}*/    	/*diff.*/
if (first<11)
  {stat=test9(); if (stat!=0) menu();}
if (first<12)
  {stat=test11(); if (stat!=0) menu();}
if (first<13)
  {stat=test12(); if (stat!=0) menu();}

close_corbo();  
return(0);
}


/**************/
int single(void)		/*execute a single test routine*/
/**************/
{
int num;

stat=init_corbo(); 
if (stat!=0) 
  menu();

do
  {
  printf(" \n");
  printf(" \n");
  printf(" \n");
  printf(" 0    = Quitter le mode 'test simple'\n");
  printf(" 1..12 = Passer les tests no. 1..12\n");
  printf("Entrer un chiffre \n");  /*default:0  min:0  max:12*/
  scanf("%d", &num);
  printf("\n");
  if(num==1) test1();
  if(num==2) test2();
  if(num==3) test3();
  if(num==4) test4();
  if(num==5) test5();
  if(num==6) test6();
  if(num==7) test7();
  if(num==8) test8();
  if(num==9) test9();
  if(num==10) test10();
  if(num==11) test11();
  if(num==12) test12();
  }
while(num);
close_corbo();
return(0);
}


/****************/
/*Initialisation*/
/****************/

/******************/
int init_corbo(void)
/******************/
{
unsigned int ret;
int ok=0,inized=0, loop;
unsigned short ldata;

if (inized==1) 
  return(0);

/*Initialise VME mapping*/
ret = VME_Open();
if (ret != VME_SUCCESS)
{
  printf(" Error in VME_Open.\n");
  exit(-1);
}

ret = VME_UniverseMap(&unibase);
if (ret != VME_SUCCESS)
{
  printf(" Error in VME_UniverseMap\n");
  exit(-1);
}
uni = (universe_regs_t *)unibase;

ret = VME_MasterMap(&master_map, &handle);
if (ret != VME_SUCCESS)
{
  printf(" Error in VME_MasterMap. Check parameters!\n");
  exit(-1);
}

/*scan crate for corbo*/
for(loop=0;loop<0x1000000;loop+=0x100)
  {
  ret = VME_ReadSafeUShort(handle ,loop, &ldata);
  if (!ret)
    {
    cbase=loop;
    ok=1;
    break;		/*exit loop on first good cycle*/
    }
  }
if(!ok)
  {
  printf("The program could not find a CORBO in the crate.\n");
  printf("This error could be due to:\n");
  printf("- The CORBO has not been completely inserted.\n");
  printf("- The CORBO (CSR1) is damaged.\n");
  printf("Please call one of the contact persons.\n");
  prtc();
  return(1);
  }
else
  printf("CORBO found at VMEbus address 0x%06x\n", cbase);
  
ret = TM_open(cbase);
if (ret)
{
  printf(" Error in TM_open. Check parameters!\n");
  return(1);
}

inized=1;
return(0);
}


/********************/
void close_corbo(void)
/********************/
{
  VME_UniverseUnmap(unibase);
  TM_close();
  VME_Close();
}


/****************************************/
/*		test1			*/
/*   check if the corbo is accessible	*/
/*   check the power up defaults	*/
/*   check the front panel LEDs		*/
/****************************************/
/*************/
int test1(void)
/*************/
{
unsigned int ret;
unsigned short ldata;
int ok=0,regs[128],loop;


printf("TEST 1\n");
printf("A CORBO was found at address Hex %x. Quit the test if it does not match with the actual set-up of the board to be tested.\n",cbase&0xffff00);
prtc();
for(loop=0;loop<0x28;loop++)  /*read registers csr1 to bim2vr3*/
  {
  ret = VME_ReadSafeUShort(handle ,cbase+2*loop, &ldata);
  if (ret)
    {
    printf(" Pas de reponse du register d'offset %x\n",2*loop);
    prtc();
    }
  else
    {
    ok++;
    regs[loop]=ldata;    
    }
  }
for(loop=0x30;loop<0x80;loop++)    	/*read static memory*/
  {
  ret = VME_ReadSafeUShort(handle ,cbase+2*loop, &ldata);
  if (ret)
    {
    printf(" Pas de reponse de la memoire statique d'offset %x\n",2*loop);
    prtc();
    }
  else
    {
    ok++;
    regs[loop]=ldata;    
    }
  }
if (ok==0)	/*no response at all*/
  {
  printf(" Le  CORBO est inaccessible !\n");
  printf(" Il n'est donc pas possible de continuer les tests.\n");
  printf(" Les raisons possibles de cette erreur sont :\n");
  printf(" - le CORBO n'est pas insere correctement\n");
  printf(" - l'adresse de base du CORBO n'est pas '0007'\n");
  printf(" - le CORBO est completement mort (le pauvre)\n");
  printf(" - le FIC8234 ne fonctiomnne pas correctement\n");
  printf(" Quitter le test et essayer de resoudre le probleme.\n");
  prtc();
  return(1);
  }
if (ok!=120)	/*no response from some addresses*/
  {
  printf(" certains registres du CORBO sont inaccessibles.\n");
  printf(" Il n'est donc pas possible de continuer les tests.\n");
  printf(" Les raisons possibles de cette erreur sont :\n");
  printf(" le CORBO n'est pas insere correctement\n");
  printf(" - l'electronique de ce CORBO est endommage\n");
  printf(" Quitter le test et essayer de resoudre le probleme.\n");
  prtc();
  return(1);
  }
else	/*full response, continue*/
  {
  /*checking power up defaults*/
  if ((regs[0]&0xff)!=0xff)
    {
    printf(" Le  registre 'CSR1' ne contient pas les valeurs par defaut du demarrage\n");
    prtc();
    }
  if ((regs[1]&0xff)!=0xff)
    {
    printf(" Le  registre 'CSR2' ne contient pas les valeurs par defaut du demarrage\n");
    prtc();
    }
  if ((regs[2]&0xff)!=0xff)
    {
    printf(" Le  registre 'CSR3' ne contient pas les valeurs par defaut du demarrage\n");
    prtc();
    }
  if ((regs[3]&0xff)!=0xff)
    {
    printf(" Le  registre 'CSR4' ne contient pas les valeurs par defaut du demarrage\n");
    prtc();
    }
  }

/*check front panel LEDs*/
/*test pattern to switch LEDs off*/
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR1, 0xf3);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR1\n");
  prtc();
  }
else
  {
  printf(" Placer l'interrrupteur DIP le plus haut du CORBO dans la position droite\n");
  printf(" Placer l'interrrupteur DIP le plus bas du CORBO dans la position droite\n");
  printf(" Appuyer sur le bouton du panneau avant\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont etteintes.\n");
  prtc();
  }
  
/*test pattern to switch LEDs on*/
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR1, 0x0c);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR1\n");
  prtc();
  }
else
  {
  ret = VME_WriteSafeUShort(handle ,cbase+VMETR_TEST1, 0xff);
  if (ret)
    {
    printf(" Il est impossible d'ecrire dans le registre TEST1\n");
    prtc();
    }
  else
    {
    printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption de la LED 'In reflect'\n");
    prtc();
    }
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR1, 0x00);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR1\n");
  prtc();
  }
else
  {
  printf(" Enlever le pont 'IN' du canal 1 du CORBO\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption des LEDs 'Sel.1'et 'Sel.2'.\n");
  prtc();
  }

ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR2, 0xf3);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR2\n");
  prtc();
  }
else
  {
  printf(" Placer l'interrrupteur DIP le plus haut du CORBO dans la position gauche\n");
  printf(" Placer l'interrrupteur DIP le plus bas du CORBO dans la position droite\n");
  printf(" Appuyer sur le bouton poussoir du panneau avant\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont etteintes.\n");
  prtc();
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR2, 0x0c);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR2\n");
  prtc();
  }
else
  {
  ret = VME_WriteSafeUShort(handle ,cbase+VMETR_TEST2, 0xff);
  if (ret)
    {
    printf(" Il est impossible d'ecrire dans le registre TEST2");
    prtc();
    }
  else
    {
    printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption de la LED 'In reflect'\n");
    prtc();
    }
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR2, 0x00);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR2\n");
  prtc();
  }
else
  {
  printf(" Enlever le pont 'IN' du canal 2 du CORBO\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption des LEDs 'Sel.1'et 'Sel.2'.\n");
  prtc();
  }

ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR3, 0xf3);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR3\n");
  prtc();
  }
else
  {
  printf(" Placer l'interrrupteur DIP le plus haut du CORBO dans la position droite\n");
  printf(" Placer l'interrrupteur DIP le plus bas du CORBO dans la position gauche\n");
  printf(" Appuyer sur le bouton poussoir du panneau avant\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont etteintes.\n");
  prtc();
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR3, 0x0c);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR3\n");
  prtc();
  }
else
  {
  ret = VME_WriteSafeUShort(handle ,cbase+VMETR_TEST3, 0xff);
  if (ret)
    {
    printf(" Il est impossible d'ecrire dans le registre TEST3\n");
    prtc();
    }
  else
    {
    printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption de la LED 'In reflect'\n");
    prtc();
    }
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR3, 0x00);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR3\n");
  prtc();
  }
else
  {
  printf(" Enlever le pont 'IN' du canal 3 du CORBO\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption des LEDs 'Sel.1'et 'Sel.2'.\n");
  prtc();
  }

ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR4, 0xf3);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR4\n");
  prtc();
  }
else
  {
  printf(" Placer l'interrrupteur DIP le plus haut du CORBO dans la position gauche\n");
  printf(" Placer l'interrrupteur DIP le plus bas du CORBO dans la position gauche\n");
  printf(" Appuyer sur le bouton poussoir du panneau avant\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont etteintes.\n");
  prtc();
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR4, 0x0c);
if (ret)
  {
  printf(" Il est impossible d'ecrire dans le registre CSR4\n");
  prtc();
  }
else
  {
  ret = VME_WriteSafeUShort(handle ,cbase+VMETR_TEST4, 0xff);
  if (ret)
    {
    printf(" Il est impossible d'ecrire dans le registre TEST4\n");
    prtc();
    }
  else
    {
    printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption de la LED 'In reflect'\n");
    prtc();
    }
  }
ret = VME_WriteSafeUShort(handle ,cbase+VMETR_CSR4, 0x00);
if (ret)
  {
  printf(" It is not possible to write to register CSR4\n");
  prtc();
  }  
else
  {
  printf(" Enlever le pont 'IN' du canal 4 du CORBO\n");
  printf(" Ce test est valide si toutes les LEDS du panneau avant sont allumes a l'exeption des LEDs 'Sel.1'et 'Sel.2'.\n");
  prtc();
  }

printf(" Replacer tous les ponts enleves lors du test\n");
prtc();
return(0);
}


/****************************************/
/*		test2			*/
/*   check if the RAM registers are OK	*/
/****************************************/
/*************/
int test2(void)
/*************/
{
int ok=1,offset;
unsigned short d1,d2;

printf("TEST 2\n");

for (offset=0;offset<=0x9e;offset+=2)  
  {
  d1=0xffff;		/*test pattern*/
  wtc(cbase+VMETR_RAM+offset,d1);	/*write pattern*/
  rfc(cbase+VMETR_RAM+offset,&d2);	/*read back*/
  if (d1!=d2)
    {
    printf(" La memore de ce CORBO est endommagee.\n");
    printf(" Adresse:     %02x\n",VMETR_RAM+offset);
    printf(" Motif ecrit: %04x\n",d1);
    printf(" Motif lu:    %04x\n",d2);
    prtc();
    ok=0;
    }
  d1=0;	/*test pattern*/
  wtc(cbase+VMETR_RAM+offset,d1);
  rfc(cbase+VMETR_RAM+offset,&d2);
  if (d1!=d2)
    {
    printf(" La memore de ce CORBO est endommagee.\n");
    printf(" Adresse:     %02x\n",VMETR_RAM+offset);
    printf(" Motif ecrit: %04x\n",d1);
    printf(" Motif lu:    %04x\n",d2);
    prtc();
    ok=0;
    }
  }
if (ok) return(0); else return(1);
}



/****************************************/
/*		test3			*/
/*   check event interrupt, triggered	*/
/*   by a front panel NIM or TTL pulse. */
/*   check with different interrupt	*/
/*   levels and vectors.		*/
/****************************************/
/*************/
int test3(void)
/*************/
{
int idata, int_handle, ve,level,count,channel,vecnum;
unsigned int ret;
VME_InterruptList_t irq_list;
VME_InterruptInfo_t ir_info;
  
printf("TEST 3\n");
printf(" Connecter le generateur d'impulsions a l'osciloscope suivant la procedure decrite dans la 'procedure d'installation 1' et generer des impulsions NIM/TTL\n");
prtc();
for (ve=1;ve<3;ve++)	/*loop for 2 vectors*/
  {
  if (ve==1) 
    {
    vecnum=102;
    printf(" Placer les ponts 'IN' des canaux 1,2,3 et 4 du panneau avant du CORBO sur la position 'N'\n");
    }
  else 
    {
    vecnum=153;
    printf(" Placer les ponts 'IN' des canaux 1,2,3 et 4 du panneau avant du CORBO sur la position 'T'\n");
    }

  for (channel=1;channel<5;channel++)	/* loop for 4 channels*/
    {
    if (ve==1)
      printf(" Connecter le canal de sortie d'impulsions NIM du generateur d'impulsion avec le canal d'entre %d du CORBO a tester.\n",channel);
    else
      printf(" Connecter le canal de sortie d'impulsions TTL du generateur d'impulsion avec le canal d'entre %d du CORBO a tester.\n",channel);
    prtc();
    for (level=1;level<8;level++)	/* loop for 7 levels*/
      {
      irq_list.number_of_items = 1;
      irq_list.list_of_items[0].vector = vecnum;
      irq_list.list_of_items[0].level = level;
      irq_list.list_of_items[0].type = VME_INT_ROAK;

      ret = VME_InterruptLink(&irq_list, &int_handle);
      if (ret != VME_SUCCESS)
        printf(" Error from VME_InterruptLink\n");
	
      TM_csrW(channel,0xe1);	  	/* initialise and disable channel*/
      TM_clbsy(channel);          	/* clear pending i/r */
      TM_bimcrW(channel,0); 		/* disable*/
      TM_cntW(channel,0);         	/* preset trigger counter */
      TM_toW(channel,0);          	/* clear counter*/
      TM_bimvrW(channel,vecnum);	/* store vector*/
      TM_bimcrW(channel,(0x58+level));	/* IR-level, autoclear*/
      TM_csrW(channel,0xe0);   		/* (re)enable triggers */
      printf(" Appuyer sur le bouton 'MAN' du generateur d'impulsions\n");
      ir_warning();

      ret = VME_InterruptWait(int_handle, VME_INTTIMEOUT, &ir_info);
      if (ret != VME_SUCCESS)
        printf(" Error from VME_InterruptWait\n");

      ret = VME_InterruptUnlink(int_handle);
      if (ret != VME_SUCCESS)
        printf(" Error from VME_InterruptUnlink\n");
	
      /*check auto clear*/
      sleep(1);
      TM_bimcrR(channel,&idata); 
      if (idata&0x50)
        {
        printf(" Autoclear n'a pas fonctionne\n");
        printf(" bimcr%d is %d\n",channel,idata);
        prtc();
        }

      TM_csrW(channel,0xe1); 	     	/* disable trigger */
      TM_cntR(channel,&count);		/* read event counter*/
      if (count!=1)
        {
        printf(" Il y a un probleme avec le compteur d'evenements du canal %d\n",channel);
        printf(" La valeur lu est Hex. %x   (au lieu de1)\n",count);
        prtc();
        }
      TM_cntW(channel,0); 		/* clear counter*/
      TM_cntR(channel,&count);		/* read back*/
      if (count!=0)
        {
        printf(" Il est impossible de remettre a zero le compteur d'evenements du canal %d\n",channel);
        prtc();
        }
      TM_clbsy(channel);               	/* triggers may arrive, but disabled*/
      TM_toR(channel,&count);		/* read time out counter*/
      if (count==0)
        {
        printf(" Probleme: le compteur de 'dead time' du canal %d est vide\n",channel);
        prtc();
        }
      }
    }
  }
return(0);
}


/****************************************/
/*		test4			*/
/*   check event interrupt triggered	*/
/*   by the front panel push button  	*/
/****************************************/
/*************/
int test4(void)
/*************/
{
int idata, int_handle, level,channel,vecnum;
unsigned int ret;
VME_InterruptList_t irq_list;
VME_InterruptInfo_t ir_info;

printf("TEST 4\n");
printf(" PLacer les ponts 'IN' des canaux 1,2,3 et 4 du panneau avant du CORBO sur la position 'N' et retirer le cable LEMO du canal 4\n");
prtc();

vecnum=153;
level=3;
irq_list.number_of_items = 1;
irq_list.list_of_items[0].vector = vecnum;
irq_list.list_of_items[0].level = level;
irq_list.list_of_items[0].type = VME_INT_ROAK;

ret = VME_InterruptLink(&irq_list, &int_handle);
if (ret != VME_SUCCESS)
  printf(" Error from VME_InterruptLink\n");

for (channel=1;channel<5;channel++)
  {
  TM_csrW(channel,0x43);	  	/* initialise and disable channel*/
  TM_clbsy(channel);          		/* clear pending i/r */
  TM_bimcrW(channel,0); 		/* disable*/
  TM_cntW(channel,0);         		/* preset trigger counter */
  TM_toW(channel,0);          		/* clear counter*/
  TM_bimvrW(channel,vecnum);		/* store vector*/
  TM_bimcrW(channel,(0x58+level));	/* IR-level, autoclear*/
  TM_csrW(channel,0x42);   		/* (re)enable triggers */
  printf(" Appuyer sur le bouton poussoir du panneau avant\n");
  ir_warning();

  ret = VME_InterruptWait(int_handle, VME_INTTIMEOUT, &ir_info);
  if (ret != VME_SUCCESS)
    printf(" Error from VME_InterruptWait\n");  /*check auto clear*/

  TM_bimcrR(channel,&idata); 
  if (idata&0x50)
    {
    printf(" Autoclear n'a pas fonctionne\n");
    printf(" bimcr%d = %d\n",channel,idata);
    prtc();
    }
  printf(" OK\n");
  TM_csrW(channel,0x43); 	     	/* disable trigger */
  TM_clbsy(channel);               	/* triggers may arrive, but disabled*/
  sleep(1);				/*mask out the bouncing of the push button*/
  TM_bimcrW(channel,(0x58+level));  	/* reenable IE (BIM) */
  }
  
ret = VME_InterruptUnlink(int_handle);
if (ret != VME_SUCCESS)
  printf(" Error from VME_InterruptUnlink\n");
  
return(0);
}



/****************************************/
/*		test5			*/
/*   check internal test registers	*/
/*   check fast clear enable/disable  	*/
/****************************************/
/*************/
int test5(void)
/*************/
{
int loop1,loop,count,channel;

printf("TEST 5\n");
printf(" Connecter le generateur d'impulsions a l'osciloscope suivant la procedure decrite dans la 'procedure d'installation 1' et generer des impulsions NIM/TTL\n");
prtc();
for(loop1=1;loop1<3;loop1++)
  { 
  printf(" Placer le pont 'CLEAR' du panneau avant du CORBO\n");
  if (loop1==1)
    {
    printf(" sur la position 'N' et connecter le connecteur LEMO a droite de cet pont avec la sortie NIM du generateur d'impulsions\n");
    }
  else
    { 
    printf(" sur la position 'T' et connecter le connecteur LEMO a droite de cet pont avec la sortie TTL du generateur d'impulsions\n");
    }
  prtc();

  for(channel=1;channel<5;channel++)
    {
    TM_csrW(channel,0xcd);	  	/* initialise and disable channel*/
    TM_clbsy(channel);         		/* clear pending i/r */
    TM_cntW(channel,0);        		/* preset trigger counter */
    TM_toW(channel,0);         		/* clear counter*/
    TM_csrW(channel,0xcc);   		/* (re)enable triggers, fast clear disabled */
  
    for(loop=1;loop<101;loop++)		/* generate 100 input pulses*/
      TM_test(channel); 		/* BUSY = active */
    sleep(1);
    TM_clbsy(channel);          	/* clear pending i/r */

    TM_toR(channel,&count);		/* read time out counter*/
    if(count>9950 || count <9750)	/* check the slow clock's freq.*/
      {
      printf(" Le compteur de 'dead time' du chanal %d\n",channel);
      printf(" compte avec une frequence proche de. %f5.1 Khz\n",(float)count/1000);
      printf(" Les causes possibles de cette erreur sont :\n");
      printf(" - L'electronique du compteur 'dead time' est endommage\n");
      printf(" - L'oscillateur de l'horloge lente a ete change\n");
      prtc();
      }

    TM_cntR(channel,&count);		/* read event counter*/
    if(count!=100)
      {
      printf(" Le compteur d'evenements du canal %d\n",channel);
      printf(" a compte %d sur 100 'triggers'\n",count);
      printf(" Les causes possibles de cette erreur sont :\n");
      printf(" - L'electronique du compteur d'evenements est endommage\n");
      prtc();
      }

    printf(" Appuyer sur le bouton 'MAN' du generateur d'impulsions\n");
    prtc();
    TM_cntR(channel,&count);		/* read event counter*/
    if(count==0)
      {
      printf(" Le compteur d'evenements du canal %d est peut etre remis a zero bien qu'il soit non active.\n",channel);
      printf(" Les causes possibles de cette erreur sont :\n");
      printf(" - L'electronique du compteur d'evenements est endommage\n");
      prtc();
      }

    TM_csrW(channel,0x8c);		/* enable fast clear*/
    printf(" Appuyer sur le bouton 'MAN' du generateur d'impulsions\n");
    prtc();
    TM_cntR(channel,&count);		/* read event counter*/
    if(count!=0)
      {
      printf(" Le compteur d'evenements du canal %d\n",channel);
      printf("  est peut etre remis a zero.\n");
      printf(" Les causes possibles de cette erreur sont :\n");
      printf(" - L'electronique du compteur d'evenements est endommage\n");
      printf(" - Le generateur d'impulsion n'est pas correctement installe\n");
      printf(" - Le pont d'entre 'CLEAR' n'est pas dans la bonne position\n");
      printf(" - Le cable LEMO est casse\n");
      prtc();
      }
    }
  }
return(0);
}


/****************************************/
/*		test6			*/
/*   check dead time counter interrupts	*/
/****************************************/
/*************/
int test6(void)
/*************/
{
int int_handle,d3,data,vector,ve,channel,level;
unsigned int ret;
VME_InterruptList_t irq_list;
VME_InterruptInfo_t ir_info;

printf("TEST 6\n");
TM_clbsy(1);                        /*clear BUSY*/
TM_clbsy(2);                        /*clear BUSY*/
TM_clbsy(3);                        /*clear BUSY*/
TM_clbsy(4);                        /*clear BUSY*/
TM_csrR(1,&data);
printf("csr 1 =%x\n",data);
TM_csrR(2,&data);
printf("csr 2 =%x\n",data);
TM_csrR(3,&data);
printf("csr 3 =%x\n",data);
TM_csrR(4,&data);
printf("csr 4 =%x\n",data);
prtc();

for (ve=1;ve<3;ve++)
  {
  if (ve==1)
    vector=102;
  else
    vector=153;
  for (channel=1;channel<2;channel++)
    {
    printf("Vector is %d   Channel is %d\n",vector,channel);
    TM_csrW(channel,0xcc);			/*enable channel*/
    TM_bimcrW(channel,0x0);			/*disable interrupt*/
    TM_bimvrW(channel,0x0);			/*store vector*/
    TM_bim2vrW(channel,vector);			/*store vector*/
    for (level=1;level<8;level++)
      {
      irq_list.number_of_items = 1;
      irq_list.list_of_items[0].vector = vector;
      irq_list.list_of_items[0].level = level;
      irq_list.list_of_items[0].type = VME_INT_ROAK;

      ret = VME_InterruptLink(&irq_list, &int_handle);
      if (ret != VME_SUCCESS)
	printf(" Error from VME_InterruptLink\n");
      
      TM_clbsy(channel);                        /*clear BUSY*/
      TM_toW(channel,0xf000);			/*preset counter*/
      TM_csrR(channel,&data);
      printf("before init bim  level=%d    csr=%x\n",level,data);
      TM_bim2crW(channel,(0x58+level));         /*initialise CSR*/
      TM_test(channel);				/*activate BUSY*/

      ir_warning();
      ret = VME_InterruptWait(int_handle, VME_INTTIMEOUT, &ir_info);
      if (ret != VME_SUCCESS)
        printf(" Error from VME_InterruptWait\n");

      TM_bim2crR(channel,&d3); 		        /*initialise CSR*/
      printf("BIM2CR[%d] = %d\n",channel,d3);
      TM_clbsy(channel);                        /*clear BUSY*/

      ret = VME_InterruptUnlink(int_handle);
      if (ret != VME_SUCCESS)
	printf(" Error from VME_InterruptUnlink\n");
      }
    }
  }
return(0);
}


/****************************************/
/*              test7                   */
/*   check the LEMO BUSY outputs	*/
/*   check whether disabled functions 	*/
/*   are realy disabled			*/
/****************************************/
/*************/
int test7(void)
/*************/
{
int t,channel;

printf("TEST 7\n");
printf(" Installer l'oscilloscope comme decrit dans les 'procedure d'installations 2'.\n");
printf(" Placer les ponts 'BUSY' des 4 canaux du CORBO sur 'N'.\n");
prtc();

for(channel=1;channel<5;channel++)
  {
  printf(" Connecter le canal 1 de l'oscilloscope au connecteur LEMO 'BUSY' du canal %d du CORBO a tester.\n",channel);
  printf(" Comparer le signal sur l'ecran de l'oscilloscope avec la Fig. 4 du manuel. Ces signaux doivent concorder aux tolerances pres. Presser sur 'ctrl+\' pour continuer.\n");
  siggi=0;
  TM_csrW(channel,0xc1);	/*initialise and disable csr*/
  TM_clbsy(channel);		/*clear ir*/
  TM_bimcrW(channel,0);		/*disable ir*/
  TM_test(channel);		/*set BUSY to active state*/
  while(!siggi)			/*wait for signal 'ctrl+\'*/
    {				/*generate first test pattern*/
    TM_clbsy(channel);		/*set BUSY to inactive stata*/
    TM_test(channel);
    for(t=3;t;t--) ;		/*a short delay*/
    }
  }

for(channel=1;channel<5;channel++)
  {
  printf(" Connecter le canal 1 de l'oscilloscope au connecteur LEMO 'BUSY' du canal %d du CORBO a tester.\n",channel);
  printf(" Comparer le signal sur l'ecran de l'oscilloscope avec la Fig. 5 du manuel. Ces signaux doivent concorder aux tolerances pres. Presser sur 'ctrl+\' pour continuer.\n");
  siggi=0;
  TM_csrW(channel,0xC1);	/*initialise and disable csr*/
  TM_clbsy(channel);		/*clear ir*/
  TM_bimcrW(channel,0);		/*disable ir*/
  TM_csrW(channel,0xEC);	/*enable csr*/
  TM_test(channel);		/*set BUSY to active state*/
  while(!siggi)			/*wait for signal 'ctrl+\'*/
    {				/*generate first test pattern*/
    TM_clbsy(channel);		/*set BUSY to inactive stata*/
    TM_test(channel);
    for(t=3;t;t--) ;		/*a short delay*/
    }
  }

printf(" Installer l'oscilloscope comme decrit dans les 'procedure d'installations 3'.\n");
printf(" Placer les ponts 'BUSY' des 4 canaux du CORBO sur 'T'.\n");
prtc();

for(channel=1;channel<5;channel++)
  {
  printf(" Connecter le canal 1 de l'oscilloscope au connecteur LEMO 'BUSY' du canal %d du CORBO a tester\n.",channel);
  printf(" Comparer le signal sur l'ecran de l'oscilloscope avec la Fig. 6 du manuel. Ces signaux doivent concorder aux tolerances pres. Presser sur 'ctrl+\' pour continuer.\n");
  siggi=0;
  TM_csrW(channel,0xC1);
  TM_clbsy(channel);
  TM_bimcrW(channel,0);
  TM_csrW(channel,0xEC);
  TM_test(channel);
  while(!siggi)
    {
    TM_clbsy(channel);
    for(t=3;t;t--) ;
    TM_test(channel);
    }
  }
return(0);
}


/************************************************/
/*		test8				*/
/*   check event interrupt, triggered		*/
/*   by a front panel differential pulse. 	*/
/************************************************/
/*************/
int test8(void)
/*************/
{
int int_handle, count,channel,vecnum;
unsigned int ret;
VME_InterruptList_t irq_list;
VME_InterruptInfo_t ir_info;

printf("TEST 8\n");
printf(" Connect le pulse generator to le osciloscope as described in \n");
printf(" 'set-up procedure 1' and set it up to produce NIM/TTL pulses\n");
printf(" as described lere.\n");
printf(" Connect le NIM-pulse output channel of le pulse generator\n");
printf(" with le 'start' connector of le NIM to ECL converter.\n");
prtc();

vecnum=102;

for (channel=1;channel<5;channel++)	/*loop for 4 channels*/
  {
  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = vecnum;
  irq_list.list_of_items[0].level = 1;
  irq_list.list_of_items[0].type = VME_INT_ROAK;

  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
    printf(" Error from VME_InterruptLink\n");
  
  printf("Connect le ECL output of le NIM to ECL converter with le ECL input of channel %d of le OCRBO.\n",channel);
  prtc();
  TM_csrW(channel,0xc5);	/* initialise and disable channel*/
  TM_clbsy(channel);          	/* clear pending i/r */
  TM_bimcrW(channel,0); 	/* disable*/
  TM_cntW(channel,0);         	/* preset trigger counter */
  TM_toW(channel,0);          	/* clear counter*/
  TM_bimvrW(channel,vecnum);	/* store vector*/
  TM_bimcrW(channel,(0x59));	/* IR-level, autoclear*/
  TM_csrW(channel,0xc4);   	/* (re)enable triggers */
  printf(" Press le 'MAN' button of le pulse generator\n");
  ir_warning();

  ret = VME_InterruptWait(int_handle, VME_INTTIMEOUT, &ir_info);
  if (ret != VME_SUCCESS)
    printf(" Error from VME_InterruptWait\n");
  
  TM_csrW(channel,0xc5); 	     	/* disable trigger */
  TM_cntR(channel,&count);		/* read event counter*/
  if (count!=1)
    {
    printf(" There is a problem with le event counter of channel %d\n",channel);
    printf(" Value read is Hex. %x  (should be 1)\n",count);
    prtc();
    }
  TM_cntW(channel,0); 			/* clear counter*/
  TM_cntR(channel,&count);		/* read back*/
  if (count!=0)
    {
    printf(" It is not possible to clear le event counter of channel %d\n",channel);
    prtc();
    }
  TM_clbsy(channel);               	/* triggers may arrive, but disabled*/
  TM_toR(channel,&count);		/* read time out counter*/
  if (count==0)
    {
    printf(" Problem: The dead time counter of channel %d is empty\n",channel);
    prtc();
    }
    
  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
    printf(" Error from VME_InterruptUnlink\n");
  }
return(0);
}



/****************************************/
/*              test9                   */
/*   check the slow clock LEMO output.  */
/*   The SW does not do any testing	*/
/*   It outpust just some text to guide */
/*   the tester.			*/
/****************************************/
/*************/
int test9(void)
/*************/
{
printf("TEST 9\n");
printf(" Installer l'oscilloscope comme decrit dans les 'procedure d'installations 4'.\n");
printf(" Placer le pont 'clock' du panneau avant du CORBO sur 'N'\n");
printf(" Connecter le canal 1 de l'osciloscope avec le connecteur LEMO se trouvant a droite de le pont 'clock'.\n");
printf(" Le signal de l'osciloscope devrait correspondrea la Fig. 7 du manuel.\n");
prtc();

printf(" Installer l'oscilloscope comme decrit dans les 'procedure d'installations 5'.\n");
printf(" Placer le pont 'clock' du panneau avant du CORBO sur 'T'\n");
printf(" Connecter le canal 1 de l'osciloscope avec le connecteur LEMO se trouvant a droite de le pont 'clock'.\n");
printf(" Le signal de l'osciloscope devrait correspondrea la Fig. 8 du manuel.\n");
prtc();
return(0);
}


/*****************************************/
/*              test10                   */
/*   check the differential BUSY outputs */
/*****************************************/
/**************/
int test10(void)
/**************/
{
int t,channel;

printf("TEST 10\n");
printf(" Set-up le osciloscope and le NIM to ECL converter as described in 'set-up procedure 6'\n");
prtc();

for(channel=1;channel<5;channel++)
  {
  printf(" Connect channel 1 of the oscilloscope with the 'OUT' LEMO connector of the NIM to ECL converter.\n");
  printf(" Connect the ECL output of channel %d of the CORBO with the 'start' ECL connector of the NIM to ECL converter.\n",channel);
  printf(" Compare the signal on the screen of the oscilloscope with Fig. 9 in the write-up. They have to match within the given tollerances. Press 'ctrl+\' when ready.\n");
  siggi=0;
  TM_csrW(channel,0xd1);	/*initialise and disable csr*/
  TM_clbsy(channel);		/*clear ir*/
  TM_bimcrW(channel,0);		/*disable ir*/
  TM_csrW(channel,0xEC);	/*enable csr*/
  TM_test(channel);		/*set BUSY to active state*/
  while(!siggi)			/*wait for signal 'ctrl+\'*/
    {				/*generate first test pattern*/
    TM_clbsy(channel);		/*set BUSY to inactive stata*/
    TM_test(channel);
    for(t=3;t;t--) ;		/*a short delay*/
    }
  }
return(0);
}

/****************************************/
/*		test11			*/
/*   check interrupt clear with		*/
/*   front panel push button		*/
/****************************************/
/**************/
int test11(void)
/**************/
{
int data,channel;

printf("TEST 11\n");

ir_off(1);					/*disable IR level 3*/

for (channel=1;channel<5;channel++)
  {
  TM_csrW(channel,0xcd);			/*disable channel*/
  TM_clbsy(channel);				/*clear BUSY*/
  TM_bimcrW(channel,0x0);			/*disable interrupt*/
  TM_bimvrW(channel,0x0);			/*store vector*/
  TM_bim2vrW(channel,102);			/*store vector*/
  TM_toW(channel,0xff00);
  TM_bim2crW(channel,0x59);               	/*initialise CSR*/
  TM_csrW(channel,0xcc);	                /*enable channel*/
  TM_test(channel);				/*activate BUSY*/
  sleep(1);
  printf("Appuyer sur le bouton poussoir du panneau avant\n");
  prtc();
  TM_csrW(channel,0xcd);              	        /*disable channel*/
  TM_csrR(channel,&data);
  if (!(data&0x800))
    {
    printf(" It was not possible to clear a pending interrupt with the front panel push button.\n");  
    printf(" Possible reasons for this error are:\n");
    printf(" The push button was not pressed carefully\n");
    printf(" This feature of the CORBO is broken\n");
    printf(" \n");
    printf(" Please repeat this sub-test later on\n");
    }
  }
ir_on(1);	/*enable IR level 3*/
return(0);
}


/****************************************/
/*		test12			*/
/*   check IR disabling			*/
/****************************************/
/**************/
int test12(void)
/**************/
{
int vec1,vec2,channel,irst1,irst2;

printf("TEST 12\n");
vec1=102;
vec2=153;
ir_warning();
  
for (channel=1;channel<5;channel++)
  {
  ir_off(1);
  ir_off(2);
  TM_csrW(channel,0xcd);			/*disable channel*/
  TM_clbsy(channel);				/*clear BUSY*/
  TM_toW(channel,0xf000);			/*preset counter*/
  TM_bimvrW(channel,vec1);			/*store vector*/
  TM_bim2vrW(channel,vec2);			/*store vector*/
  TM_bimcrW(channel,0x49);			/*disable CSR; ir level=1*/
  TM_bim2crW(channel,0x4a);         		/*disable CSR; ir level=2*/
  TM_csrW(channel,0xcc);                    	/*enable channel*/
  TM_test(channel);				/*activate BUSY*/
  sleep(2);					/*wait for BUSY overflow*/

  irst1=ir_test(1);				/*was there an IR?*/
  if (irst1==1)					/*yes --> error*/
    {
    printf("L'interruption d'evenement du canal %d ne peut pas etre desactive\n",channel);
    prtc();
    }
  irst2=ir_test(2);				/*was there an IR?*/
  if (irst2==1)					/*yes --> error*/
    {
    printf("TL'interruption de 'BUSY overflow' du canal %d ne peut pas etre desactive\n",channel);
    prtc();
    }

  TM_csrW(channel,0xcd);                      	/*disable channel*/
  ir_on(1);
  ir_on(2);
  sleep(1);
  }
return(0);
}


/****************************************************************/
/* Auxiliary routine (read word from CORBO) 			*/
/* This routine, which is able to ignore bus errors		*/
/* will be used for the first access to a register. 		*/
/* For furler accesses will be assumed that le register		*/
/* is OK and a less hevy access routine (TM_xxx) will be used	*/
/****************************************************************/
/**************************************************/
void rfc(unsigned int address, unsigned short *data)
/**************************************************/
{
unsigned int ret;

ret = VME_ReadSafeUShort(handle ,address, data);
if (ret)
  {
  printf(" La lecture d'un mot sur le CORBO est impossible\n");
  printf(" Il est impossible de continuer.\n");
  printf(" Les causes possibles de cette erreur sont :\n");
  printf(" - le CORBO n'a pas ete insere correctement\n");
  printf(" - l'electronique de ce CORBO est endommage\n");
  prtc();
  ex(0);
  }
}

/****************************************************************/
/* Auxiliary routine (write word to CORBO) 			*/
/* This routine, which is able to ignore bus errors		*/
/* will be used for the first access to a register. 		*/
/* For furler accesses will be assumed that le register		*/
/* is OK and a less hevy access routine (TM_xxx) will be used	*/
/****************************************************************/
/*************************************************/
void wtc(unsigned int address, unsigned short data)
/*************************************************/
{
unsigned int ret;

ret = VME_WriteSafeUShort(handle ,address, data);
if (ret)
  {
  printf(" La lecture d'un mot sur le CORBO est impossible\n");
  printf(" Il est impossible de continuer.\n");
  printf(" Les causes possibles de cette erreur sont :\n");
  printf(" - le CORBO n'a pas ete insere correctement\n");
  printf(" - l'electronique de ce CORBO est endommage\n");
  prtc();
  ex(0);
  }
}


/****************************************************************/
/* Auxiliary routine to enable and disable individual		*/
/* interrupt levels in the Universe chip and to check for	*/
/* active levels						*/
/****************************************************************/
/*******************/
void ir_on(int level)
/*******************/
{
unsigned int value, mask;

if (level < 1 || level > 7)
  exit(-2);
  
value = uni->lint_en;
mask = 1 << level;
value |= mask;
uni->lint_en = value;
}


/********************/
void ir_off(int level)
/********************/
{
unsigned int value, mask;

if (level < 1 || level > 7)
  exit(-2);
  
value = uni->lint_en;
mask = ~(1 << level);
value &= mask;
uni->lint_en = value;
}

/********************/
int ir_test(int level)
/********************/
{
unsigned int value, mask;

if (level < 1 || level > 7)
  exit(-2);
  
value = uni->lint_stat;
mask = 1 << level;
if (value && mask)
  return(1);
else
  return(0);
}


/*******************/
void ir_warning(void)       /*just some text*/
/*******************/
{
printf(" Presser 'ctrl+C si rien n'apparait a l'ecran d'ici 10s. Diagnostic: une interruption est en attente  Action: redemarrer le programe\n");
}


/**************/
void ex(int num)
/**************/
{
printf("Have a nice day.\n");
close_corbo();
exit(num);
}


/**************/
void prtc(void)
/**************/
{
  char dummy[10];

  printf("Presser la touche RETURN pour continuer\n");
  fgets(dummy, 10, stdin);
}


/****************************************************************/
/* This signal interception handler is necessary to get out of	*/
/* some endless loops in 'test7'. It terminates the loop on a	*/
/* 'ctrl+\' signal. All other signals will be treated in the	*/
/* default way.							*/
/****************************************************************/
/***********************/
void icpt(int /*signal*/)
/***********************/
{
  siggi=1;		/*set flag*/
}

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
// Install signal handler for SIGQUIT
sigemptyset(&sa.sa_mask);
sa.sa_flags   = 0;
sa.sa_handler = icpt;

// Dont block in intercept handler
if (sigaction(SIGQUIT, &sa, NULL) < 0)
{
  printf("sigaction() FAILED with ");
  int code = errno;
  if (code == EFAULT) printf("EFAULT\n");
  if (code == EINVAL) printf("EINVAL\n");
  exit (-1);
}

if (argc != 1)	/*type 'corbo -?' at the command line to get this text*/
  {
  printf("Ceci est la Version 1.1 du programme de test pour le ");
  printf("CES RCB8047 CORBO. (version officielle supporte par ESS/OS)");
  ex(0);
  }
menu();
exit(0);
}

