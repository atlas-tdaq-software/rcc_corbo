/* file: vmetrig_test12.c 

  This program tests the interrupt part of the RCC VMEbus library/driver using
  as an interrupt source a CORBO interrupt module.

  The program uses from one to four channels of the CORBO to allow testing
  multiple interrupts.

  The program supports only signals  

  The program also measures the number of interrupts handled per second.

  96/07/08  J.O.Petersen ECP
  000121    adapt to IOM/Atlas
  000124    generalise to any CORBO channel 
  01/11/08  port to RCC  Linux
  01/11/19  merge semaphore & signal versions 
  01/12/13  add processing simulation 
  02/04/18  support for RORA 
  02/05/29  new IR API 
  02/05/31  test multiple VME_InterruptLink 

*************************************************************************/

#include <stdio.h>
#include <signal.h>

#include "rcc_error.h"
#include "ROSGetInput/get_input.h"
#include "tstamp.h"

#include "vme_rcc.h"
#include "rcc_corbo.h"

#ifdef DEBUG
#define DEBUG(x)        {printf x; fflush(stdout);}
#else
#define DEBUG(x)
#endif

// CORBO definitions - see the CORBO manual for details ..

/*
   (internal) busy enabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define INI_CSR1 0xc0

/*
  (internal) busy disabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define DIS_CSR1 0xc1

// interrupt enable (IRE), autoclear, level 0
#define INI_BIMCR0 0x18

// interrupt disable (level 0)
#define DIS_BIMCR0 0x00

#define VME_VECTOR_DEF 130
#define SIGNUM0 42 		// Qed RT signal
#define SIGNUM1 43 

static sigset_t empty_mask;
static sigset_t corbo_mask;
static struct sigaction sa;

static int ini_bimcr0 = INI_BIMCR0;

static int int_handle0;
static int int_handle1;

static int first_chan;
static int int_level, int_mode;

static int corbo_signal_intercept_count0 = 0;
static int corbo_signal_intercept_count1 = 0;
static int suspend_count = 0;
static int control_c = 0;

/**********************************************************/
void sigint_handler(int signum)
/**********************************************************/
{
  if (signum == SIGINT)
  {
    DEBUG((" sigint_handler: signal # = %d\n",signum));
    control_c = 1;
  }
  else 
  {
    printf("unexpected signal\n") ;
  }

}
/**********************************************************/
void corbo_signal_handler0(int signum)
/**********************************************************/
{
  VME_ErrorCode_t ret;
  VME_InterruptInfo_t ir_info;
  int cur_chan;

  DEBUG((" corbo_signal_handler0: CORBO signal # %d\n",signum));

  if (signum == SIGNUM0 )
  {
    corbo_signal_intercept_count0++;
  }
  else 
  {
    printf("unexpected signal\n") ;
  }

  ret = VME_InterruptInfoGet(int_handle0, &ir_info);	// ACK the interrupt
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  cur_chan = first_chan + (ir_info.vector - VME_VECTOR_DEF);
  
  DEBUG((" corbo_signal_handler0: CORBO channel = %d\n",cur_chan));

  TM_csrW(cur_chan,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
  TM_clbsy(cur_chan);		/* triggers may arrive, but disabled  */

  TM_bimcrW(cur_chan,ini_bimcr0); 	/* reenable IE (BIM) */
  TM_csrW(cur_chan,INI_CSR1); 	/* (re)enable triggers */

  if (int_mode == VME_INT_RORA)	/* the IR was disabled in the IRQ handler */
  {
    ret = VME_InterruptReenable(int_handle0);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

}

/**********************************************************/
void corbo_signal_handler1(int signum)
/**********************************************************/
{
  VME_ErrorCode_t ret;
  VME_InterruptInfo_t ir_info;
  int cur_chan;

  DEBUG((" corbo_signal_handler1: CORBO signal # %d\n",signum));

  if (signum == SIGNUM1 )
  {
    corbo_signal_intercept_count1++;
  }
  else 
  {
    printf("unexpected signal\n") ;
  }

  ret = VME_InterruptInfoGet(int_handle1, &ir_info);	// ACK the interrupt
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  cur_chan = first_chan + (ir_info.vector - VME_VECTOR_DEF);
  
  DEBUG((" corbo_signal_handler1: CORBO channel = %d\n",cur_chan));

  TM_csrW(cur_chan,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
  TM_clbsy(cur_chan);		/* triggers may arrive, but disabled  */

  TM_bimcrW(cur_chan,ini_bimcr0); 	/* reenable IE (BIM) */
  TM_csrW(cur_chan,INI_CSR1); 	/* (re)enable triggers */

  if (int_mode == VME_INT_RORA)	/* the IR was disabled in the IRQ handler */
  {
    ret = VME_InterruptReenable(int_handle1);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

}

/**********************************************************/
int main(void)
/**********************************************************/
{

  VME_ErrorCode_t ret;
  int i,j,k,l,istat;
  int no_chan;
  int timeout;
  int nloops; 
  VME_InterruptList_t irq_list0;
  VME_InterruptList_t irq_list1;

  unsigned int io_base_add = 0x700000;		// canonical CORBO VME address

  char ich = 'y';
  char ichstr_sig[10];

  tstamp ts1, ts2;
  float delta, usec_per_int;
 
  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler = corbo_signal_handler0;
// dont block any signal in intercept handler
  if (sigaction( SIGNUM0, &sa, NULL) < 0 ) {
      perror(" sigaction SIGNUM0 ");
      exit(1);
  }
  sa.sa_handler = corbo_signal_handler1;
// dont block any signal in intercept handler
  if (sigaction( SIGNUM1, &sa, NULL) < 0 ) {
      perror(" sigaction SIGNUM1 ");
      exit(1);
  }

// block CORBO signals everywhere EXCEPT in sigsuspend 
  sigemptyset(&corbo_mask);
  sigaddset(&corbo_mask, SIGNUM0);
  sigaddset(&corbo_mask, SIGNUM1);
  sigprocmask(SIG_BLOCK, &corbo_mask, NULL);


// SIGINT handler
  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler =  sigint_handler;
  if (sigaction( SIGINT, &sa, NULL) < 0 ) {
      perror(" sigaction SIGINT ");
      exit(1);
  }
 
  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf(" ROAK or RORA interrupt (ROAK = %2d  RORA = %2d)",
         VME_INT_ROAK,VME_INT_RORA);
  
  int_mode = getdecd(VME_INT_ROAK);

  printf(" interrupt level = ");
  int_level = getdecd(3);

  ini_bimcr0 = INI_BIMCR0 + int_level;

  ret = TM_open(io_base_add);
  if( ret != CBE_OK)
    rcc_error_print(stdout, ret);

  printf(" First CORBO channel ");
  first_chan = getdecd(1);

  no_chan = 2;
  printf(" # CORBO channels = %d \n", no_chan);

  irq_list0.number_of_items = 1;
  irq_list0.list_of_items[0].vector = VME_VECTOR_DEF;
  irq_list0.list_of_items[0].level = int_level;
  irq_list0.list_of_items[0].type = int_mode;

  irq_list1.number_of_items = 1;
  irq_list1.list_of_items[0].vector = VME_VECTOR_DEF + 1;
  irq_list1.list_of_items[0].level = int_level;
  irq_list1.list_of_items[0].type = int_mode;

  for (i = 0; i < no_chan; i++)
  {
// clear from out to in, enable from in to out 
    TM_csrW(first_chan+i,DIS_CSR1);	// disable (int) busy eqv trigger i/p 
    TM_clbsy(first_chan+i);		// clear pending i/r 
    TM_bimcrW(first_chan+i,DIS_BIMCR0);

    TM_bimcrW(first_chan+i,ini_bimcr0);
  }

  TM_bimvrW(first_chan+0,irq_list0.list_of_items[0].vector);
  TM_bimvrW(first_chan+1,irq_list1.list_of_items[0].vector);

  ret = VME_InterruptLink(&irq_list0, &int_handle0);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" Interrupt Handle = %d\n", int_handle0);

  ret = VME_InterruptLink(&irq_list1, &int_handle1);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" Interrupt Handle = %d\n", int_handle1);

  ret = VME_InterruptRegisterSignal(int_handle0, SIGNUM0);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_InterruptRegisterSignal(int_handle1, SIGNUM1);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  while (ich == 'y')
  {

    corbo_signal_intercept_count0 = 0;
    corbo_signal_intercept_count1 = 0;
    suspend_count = 0;

    printf(" # loops = ");
    nloops = getdecd(1000);

    ret = ts_clock(&ts1);
    if (ret)
      rcc_error_print(stdout, ret);

    if (int_mode == VME_INT_RORA)	/* the IR may be in a disabled state */
    {
      ret = VME_InterruptReenable(int_handle0);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      
      ret = VME_InterruptReenable(int_handle1);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }

    for (i = 0; i < no_chan; i++)
    {
      TM_csrW(first_chan+i,INI_CSR1); 	// (re)enable triggers
    }

    for (i = 1; i <= nloops; i++) 
    {

      sigsuspend(&empty_mask);   // wait for any signal
      suspend_count++;

      if (control_c)
      {
        printf(" CTL/C seen\n");
        control_c = 0;
        break;
      }

    }  // for

    ret = ts_clock(&ts2);
    if (ret)
      rcc_error_print(stdout, ret);
    delta = ts_duration(ts1, ts2);
    printf(" elapsed time : %.1f secs \n", delta);
    usec_per_int = (delta/nloops) * 1000000;
    printf(" # microsecs per interrupt = %6.2f\n",usec_per_int);

    printf(" # signals seen in handler for CORBO channel 1 = %d\n",
            corbo_signal_intercept_count0);
    printf(" # signals seen in handler for CORBO channel 2 = %d\n",
            corbo_signal_intercept_count1);

    printf(" # suspend calls = %d\n", suspend_count) ;

    printf(" the sum of the corbo_signal_intercept may be larger than the \n");
    printf(" the suspend count: more interrupts handled per suspend  \n");
    printf(" NO synchronisation between the signal handlers & the main! \n");

    printf(" restart  (y/n) : ");
    ich = getfstchar();

  } /* while*/

//  clean everything

  if (int_mode == VME_INT_RORA)	/* the IR was disabled in the IRQ handler */
  {
    ret = VME_InterruptReenable(int_handle0);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);

    ret = VME_InterruptReenable(int_handle1);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

  ret = VME_InterruptUnlink(int_handle0);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_InterruptUnlink(int_handle1);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = TM_close();
  if( ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Close OK\n");

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  return 1;

}
