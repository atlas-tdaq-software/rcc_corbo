/* VME trigger module test program : vmetrig_test2_sig.c 

   same as vmetrig_test1 BUT using signals instead of semaphores

   triggers via front panel  NIM input channel2 

NB! the VME interrupt level must be enabled eg. :  /bin/ces/vmeconfig

  The program measures the number of interrupts handled per second
  and has facilities for time stamping

 96/07/15  J.Petersen ECP
           major rewrite for Lynx
           NB! the NO_HANDLER flag has to await full POSIX.4a !!

     
*/

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
/*#include <file.h>*/
#include "gpihlib.h"
#include "vmetrig.h"

int getdec();
char getfstchar();

/* now the CORBO stuff */
#define CHAN_ONE 1
#define CHAN_TWO 2
#define CHAN_THREE 3
#define CHAN_FOUR 4

/* (internal) busy enabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define INI_CSR1 0xc0

/* (internal) busy disabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define DIS_CSR1 0xc1

/* interrupt enable (IRE), autoclear, level 3 */
#define INI_BIMCR0 0x1b

/* interrupt disable (level 0) */
#define DIS_BIMCR0 0x00

/* link parameters */
#define VEC_NUM 131

/* queued signal */
#define SIGNO 45

#ifdef TIMING
static int cram_offset = 0x0;
static int cram_data;
static int cram_stat;
#endif

static int corbo_signal_intercept_count = 0;
static int corbo_signal_waitinfo_count = 0;
static int corbo_signal_pending = 0;      /* flag : 0 or 1 */
static int suspend_count = 0;
static int wait_count = 0;
static int control_c = 0;

static sigset_t empty_mask;
static sigset_t corbo_mask;
static struct sigaction sa;

#ifdef NO_HANDLER
static sigset_t look_for_these;
static siginfo_t extra;
#endif

/* gets signal via driver - kill */
void corbo_signal_handler(int signum)
{
#ifdef TIMING
    cram_data = 0x21;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

    if (signum == SIGNO )  {
        corbo_signal_intercept_count++;
        corbo_signal_pending++;
    }
    else if (signum == SIGINT) {
        printf(" seen SIGINT \n");
        control_c = 1;
    }
    else {
        printf("unexpected signal\n") ;
    }
#ifdef TIMING
    cram_data = 0x29;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

}

void wait_flag(int *flag)
{
#ifdef TIMING
    cram_data = 0x11;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

    wait_count++;

    while (*flag == 0) {
        sigsuspend(&empty_mask);   /* wait for any signal */
        suspend_count++;
    }
   (*flag)--;

#ifdef TIMING
    cram_data = 0x19;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

}

main()
{

int pathnum,vecnum0,signum0,linkid0,istat;
int npending;
int count;
int i,j;
unsigned int io_base_add=0x700000;

char ich = 'y';
time_t tbeg,tend;
int t;
int nloops = 100;               /* default # pulses */

#ifdef NO_HANDLER
    sigemptyset(&look_for_these) ;
    sigaddset(&look_for_these, SIGNO);
    sigaddset(&look_for_these, SIGINT);
#endif

    sigemptyset(&sa.sa_mask) ;
    sa.sa_flags = 0;
    sa.sa_handler =  corbo_signal_handler;
/* dont block any signal in intercept handler */
    if (sigaction( SIGNO, &sa, NULL) < 0 ) {
        perror(" sigaction GL_Signal ");
        exit(1);
    }

    if (sigaction(SIGINT, &sa, NULL) < 0 ) {
        perror(" sigaction SIGINT ");
        exit(1);
    }

    if( TM_open(io_base_add) != 0 ) {
      printf(" TM_Open error : vme base address defined ?\n");
      exit(0);
    }

/* block CORBO signals everywhere EXCEPT in sigsuspend */
    sigaddset(&corbo_mask, SIGNO);
    sigprocmask(SIG_BLOCK, &corbo_mask, NULL);

/* clear from out to in, enable from in to out */
    TM_csrW(CHAN_TWO,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
    TM_clbsy(CHAN_TWO);		/* clear pending i/r */
    TM_bimcrW(CHAN_TWO,DIS_BIMCR0);

    TM_bimvrW(CHAN_TWO,VEC_NUM);
    TM_bimcrW(CHAN_TWO,INI_BIMCR0);
/*	TM_csrW(CHAN_TWO,INI_CSR1);*/	/* enable triggers : after linking */

/* now the CORBO hardware is ready */
    istat = gpih_open("/dev/gpih",&pathnum);
    if(istat != 0) {
      printf(" gpih_open error %d errno = %d :",istat, errno);
      exit(0);
    }
    else
      printf(" Opened path number %4d\n",pathnum);

    vecnum0 = VEC_NUM;signum0 = SIGNO;
    istat = gpih_link(pathnum,GPIH_VME,vecnum0,signum0,&linkid0);
    if (istat != 0) {
      printf(" gpih_link error %d errno = %d \n", istat, errno);
      exit(0);
    }

    printf(" gpih_link on first link : status =%4d\n",istat);
    printf(" vect =%4d signal # =%6d\n",vecnum0,signum0);
    printf(" linkid =%4d\n",linkid0);

    while (ich == 'y') {

      corbo_signal_intercept_count = 0;
      corbo_signal_waitinfo_count = 0;
      corbo_signal_pending = 0;
      suspend_count = 0;
      wait_count = 0;

      printf(" # loops = ");
      nloops = getdec();
      time(&tbeg);

      for ( i=1;i<=nloops;i++) {

        TM_csrW(CHAN_TWO,INI_CSR1); 	/* (re)enable triggers */

#ifdef NO_HANDLER
        sigwaitinfo(&look_for_these, &extra);
        corbo_signal_waitinfo_count++;
#else
        wait_flag(&corbo_signal_pending);
#endif
        if(control_c)  break;

        TM_csrW(CHAN_TWO,DIS_CSR1);    /* disable (int) busy eqv trigger i/p */
        TM_clbsy(CHAN_TWO);    	       /* triggers may arrive, but disabled  */
        TM_bimcrW(CHAN_TWO,INI_BIMCR0);	/* reenable IE (BIM) */
      }

      control_c = 0;
      printf(" # signals seen in handler = %d\n",
              corbo_signal_intercept_count);
      printf(" # signals seen after waitinfo = %d\n",
               corbo_signal_waitinfo_count);
      printf(" # suspend calls = %d\n", suspend_count) ;
      printf(" # wait counts = %d\n", wait_count) ;

      time(&tend);
      t = difftime(tend, tbeg);
      printf(" elapsed time : %.2lf secs \n", (double) t);
      if (t) printf(" # loops per second  : %d \n",
              corbo_signal_intercept_count / t);

      printf(" restart  (y/n) : ");
      ich = getfstchar();

    } /* while*/

/*  clean everything */
    istat = gpih_unlink(pathnum,linkid0);
    if (istat != 0) {
      printf(" gpih_unlink error %d errno = %d \n", istat, errno);
      exit(0);
    }

    istat = gpih_close(pathnum);
    if (istat == -1)  {
     printf(" Close error %d :",errno);
    }
    else
      printf(" Closed path number %4d\n",pathnum);

    TM_close();

}

int getdec()  /*  get ONE decimal integer */

{
        char sbuf[10];   /* max 9 chars */
        int nfield,nint;

        do {
                fgets(sbuf,10,stdin);
                nfield = sscanf(sbuf,"%d",&nint);
                if (nfield<1) printf(" ??? : ");
           } while (nfield<1);

        return(nint);
}

char getfstchar()  /*  get first character */
		   /* simpler with int !! */	

{
	char sbuf[10];   /* max 9 chars */

		fgets(sbuf,10,stdin);

	return(sbuf[0]);
}
