/* VME trigger module test program : vmetrig_test1_sig.c 

   triggers via CORBO front panel NIM input channel 1

   same as vmetrig_test1 BUT using signals instead of semaphores 

NB! the VME interrupt level must be enabled 

  The program also measures the number of interrupts handled per second.

  96/07/08  J.O.Petersen ECP
  000121    adapt to IOM/ATlas
  000124    generalise to any CORBO channel 
  01/11/10  port to RCC 

*/

#include <stdio.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "iom_uio.h"
#include "vme_rcc/vme_rcc.h"
#include "rcc_corbo.h"

/* CORBO definitions */
#define CHAN_ONE 1
#define CHAN_TWO 2
#define CHAN_THREE 3
#define CHAN_FOUR 4

/* (internal) busy enabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define INI_CSR1 0xc0

/* (internal) busy disabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define DIS_CSR1 0xc1

/* interrupt enable (IRE), autoclear, level 3 */
#define INI_BIMCR0 0x1b

/* interrupt disable (level 0) */
#define DIS_BIMCR0 0x00

#define VME_VECTOR_DEF 130
#define SIGNUM  42		// Qed signal

static sigset_t empty_mask;
static sigset_t corbo_mask;
static struct sigaction sa;

static int corbo_signal_intercept_count = 0;
static int corbo_signal_waitinfo_count = 0;
static int corbo_signal_pending = 0;      /* flag : 0 or 1 */
static int suspend_count = 0;
static int wait_count = 0;
static int control_c = 0;

#ifdef NO_HANDLER
static sigset_t look_for_these;
static siginfo_t extra;
#endif

/* gets signal via driver - kill */
void corbo_signal_handler(int signum)
{
    if (signum == SIGNUM )  {
        corbo_signal_intercept_count++;
        corbo_signal_pending++;
    }
    else if (signum == SIGINT) {
        printf(" seen SIGINT \n");
        control_c = 1;
    }
    else {
        printf("unexpected signal\n") ;
    }

}

void wait_flag(int *flag)
{
    wait_count++;

    while (*flag == 0) {
        sigsuspend(&empty_mask);   /* wait for any signal */
        suspend_count++;
    }
   (*flag)--;

}

int main(void)
{

  VME_ErrorCode_t ret;
  int istat;
  int i;

  int cur_chan, cur_vecnum;

  int int_handle;
  int timeout, level, nvectors;
  int nloops; 
  u_char vector;
  u_char vmebus_vector_list[10];        // VME_MAX_VECTOR

  unsigned int io_base_add = 0x700000;

  char ich = 'y';
  char iarch[10];

  tstamp ts1, ts2;
  float delta, usec_per_int;
 
#ifdef NO_HANDLER
  sigemptyset(&look_for_these) ;
  sigaddset(&look_for_these, SIGNO);
  sigaddset(&look_for_these, SIGINT);
#endif

  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler =  corbo_signal_handler;
/* dont block any signal in intercept handler */
  if (sigaction( SIGNUM, &sa, NULL) < 0 ) {
      perror(" sigaction GL_Signal ");
      exit(1);
  }

  if (sigaction(SIGINT, &sa, NULL) < 0 ) {
      perror(" sigaction SIGINT ");
      exit(1);
  }

/* block CORBO signals everywhere EXCEPT in sigsuspend */
  sigaddset(&corbo_mask, SIGNUM);
  sigprocmask(SIG_BLOCK, &corbo_mask, NULL);

  printf(" CORBO channel # (1/2/3/4) ");
  cur_chan = getdecd(CHAN_ONE);

  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = TM_open(io_base_add);
  if( ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Open OK\n");

  printf("# vectors = ");
  nvectors = getdecd(1);
  for (i=0; i < nvectors; i++)
  {
    printf("vector = ");
    vmebus_vector_list[i] = getdecd(VME_VECTOR_DEF+i);
  }

  printf("timeout (in ms) = ");
  timeout = getdecd(10000);

/* clear from out to in, enable from in to out */
  TM_csrW(cur_chan,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
  TM_clbsy(cur_chan);		/* clear pending i/r */
  TM_bimcrW(cur_chan,DIS_BIMCR0);

  TM_bimvrW(cur_chan,vmebus_vector_list[0]);	// FIXME
  TM_bimcrW(cur_chan,INI_BIMCR0);

  ret = VME_InterruptLink(vmebus_vector_list, nvectors, &int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" IRQ Handle = %d\n", int_handle);

  ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  while (ich == 'y')
  {
    corbo_signal_intercept_count = 0;
    corbo_signal_waitinfo_count = 0;
    corbo_signal_pending = 0;
    suspend_count = 0;
    wait_count = 0;

    printf(" # loops = ");
    nloops = getdecd(1000);

    ret = ts_clock(&ts1);
    if (ret)
      rcc_error_print(stdout, ret);

    for (i=1; i<=nloops; i++) 
    {
      TM_csrW(cur_chan,INI_CSR1); 	/* (re)enable triggers */

#ifdef NO_HANDLER
      sigwaitinfo(&look_for_these, &extra);
      corbo_signal_waitinfo_count++;
#else
      wait_flag(&corbo_signal_pending);
#endif
      if(control_c)  break;

      TM_csrW(cur_chan,DIS_CSR1);	/* disable (int) busy eqv trigger i/p */ 
      TM_clbsy(cur_chan);		/* triggers may arrive, but disabled  */

      TM_bimcrW(cur_chan,INI_BIMCR0); 	/* reenable IE (BIM) */
    } /* for */

    ret = ts_clock(&ts2);
    if (ret)
      rcc_error_print(stdout, ret);
    delta = ts_duration(ts1, ts2);
    printf(" elapsed time : %.1f secs \n", delta);
    usec_per_int = (delta/nloops) * 1000000;
    printf(" # microsecs per interrupt = %6.2f\n",usec_per_int);

    control_c = 0;
    printf(" # signals seen in handler = %d\n",
            corbo_signal_intercept_count);
    printf(" # signals seen after waitinfo = %d\n",
             corbo_signal_waitinfo_count);
    printf(" # suspend calls = %d\n", suspend_count) ;
    printf(" # wait counts = %d\n", wait_count) ;

    printf(" restart  (y/n) : ");
    ich = getfstchar();

  } /* while*/

/*  clean everything */

  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = TM_close();
  if( ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Close OK\n");

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  return 1;

}
