/* VME trigger module test program : vmetrig_test1_12_sig.c 

   same as vmetrig_test1 BUT using signals instead of events

   triggers via front panel  NIM input channel 1
                             AND input channel 2

  93/02/04  J.O.Petersen ECP/DS

NB! the VME interrupt level must be enabled eg. :  /bin/ces/vmeconfig

 96/07/15  major rewrite for Lynx
           NB! the NO_HANDLER flag has to await full POSIX.4a !!

 96/07/19  generalise to two input channels

     
*/

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include "gpihlib.h"
#include "vmetrig.h"

int getdec();
char getfstchar();

/* now the CORBO stuff */
#define CHAN_ONE 1
#define CHAN_TWO 2
#define CHAN_THREE 3
#define CHAN_FOUR 4

/* (internal) busy enabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define INI_CSR1 0xc0

/* (internal) busy disabled, busy latch, front panel input, internal busy out,
   count triggers, disable external counter clear, pushbutton resets BIM
*/

#define DIS_CSR1 0xc1

/* interrupt enable (IRE), autoclear, level 3 */
#define INI_BIMCR0 0x1b

/* interrupt disable (level 0) */
#define DIS_BIMCR0 0x00

/* link parameters */
#define VEC_NUM0 130
#define VEC_NUM1 131

/* queued signals */
#define SIGNO0 44
#define SIGNO1 45

#ifdef TIMING
static int cram_offset = 0x0;
static int cram_data;
static int cram_stat;
#endif

static int corbo_signal_intercept_count = 0;

static int corbo_signal_intercept_count0 = 0;
static int corbo_signal_main_count0 = 0;
static int corbo_signal_pending0 = 0;      /* flag : 0 or 1 */

static int corbo_signal_intercept_count1 = 0;
static int corbo_signal_main_count1 = 0;
static int corbo_signal_pending1 = 0;      /* flag : 0 or 1 */

static int suspend_count = 0;
static int corbo_signal_waitinfo_count = 0;
static int control_c = 0;

static sigset_t empty_mask;
static sigset_t corbo_mask;
static struct sigaction sa;

#ifdef NO_HANDLER
static sigset_t look_for_these;
static siginfo_t extra;
#endif

/* gets signal via driver - kill */
void corbo_signal_handler0(int signum)
{
#ifdef TIMING
    cram_data = 0x21;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

    if (signum == SIGNO0 )  {
        TM_csrW(CHAN_ONE,DIS_CSR1);    /* disable (int) busy eqv trigger i/p */ 
#ifdef DEBUG
	  printf(" signal on channel 1 seen \n");
#endif
        corbo_signal_intercept_count0++;
        TM_clbsy(CHAN_ONE);                    /* clear pending i/r */
        TM_bimcrW(CHAN_ONE,INI_BIMCR0); 	/* reenable IE (BIM) */
        TM_csrW(CHAN_ONE,INI_CSR1); 		/* (re)enable triggers */
    }
    else {
        printf("unexpected signal\n") ;
    }
#ifdef TIMING
    cram_data = 0x29;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif
}

/* gets signal via driver - kill */
void corbo_signal_handler1(int signum)
{
#ifdef TIMING
    cram_data = 0x31;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif

    if (signum == SIGNO1 )  {
        TM_csrW(CHAN_TWO,DIS_CSR1);    /* disable (int) busy eqv trigger i/p */ 
#ifdef DEBUG
	  printf(" signal on channel 1 seen \n");
#endif
        corbo_signal_intercept_count1++;
        TM_clbsy(CHAN_TWO);                    /* clear pending i/r */
        TM_bimcrW(CHAN_TWO,INI_BIMCR0); 	/* reenable IE (BIM) */
        TM_csrW(CHAN_TWO,INI_CSR1); 		/* (re)enable triggers */
    }
    else {
        printf("unexpected signal\n") ;
    }
#ifdef TIMING
    cram_data = 0x39;
    cram_stat = TM_ramW(cram_offset,cram_data);
#endif
}

/* gets signal via driver - kill */
void sigint_handler(int signum)
{
    if (signum == SIGINT )  {
        control_c = 1;
    }
    else {
        printf("unexpected signal\n") ;
    }
}

main()
{
int pathnum, istat;
int vecnum0,signum0,linkid0;
int vecnum1,signum1,linkid1;
int npending;
int count;
int i,j;
unsigned int io_base_add=0x700000;

char ich = 'y';
time_t tbeg,tend;
int t;
int nloops = 100;               /* default # pulses */

#ifdef NO_HANDLER
    sigemptyset(&look_for_these) ;
    sigaddset(&look_for_these, SIGNO0);
    sigaddset(&look_for_these, SIGNO1);
    sigaddset(&look_for_these, SIGINT);
#endif

    sigemptyset(&sa.sa_mask) ;
    sa.sa_flags = 0;
    sa.sa_handler =  corbo_signal_handler0;
/* dont block any signal in intercept handler */
    sa.sa_handler =  corbo_signal_handler0;
    if (sigaction( SIGNO0, &sa, NULL) < 0 ) {
        perror(" sigaction SIGNO0 ");
        exit(1);
    }
    sa.sa_handler =  corbo_signal_handler1;
    if (sigaction( SIGNO1, &sa, NULL) < 0 ) {
        perror(" sigaction SIGNO1");
        exit(1);
    }
    sa.sa_handler =  sigint_handler;
    if (sigaction(SIGINT, &sa, NULL) < 0 ) {
        perror(" sigaction SIGINT ");
        exit(1);
    }

    if( TM_open(io_base_add) != 0 ) {
      printf(" TM_Open error : vme base address defined ?\n");
      exit(0);
    }

/* block CORBO signals everywhere EXCEPT in sigsuspend */
    sigemptyset(&corbo_mask) ;
    sigaddset(&corbo_mask, SIGNO0);
    sigaddset(&corbo_mask, SIGNO1);
    sigprocmask(SIG_BLOCK, &corbo_mask, NULL);

/* prepare TWO channels */
/* clear from out to in, enable from in to out */
      TM_csrW(CHAN_ONE,DIS_CSR1);      /* disable (int) busy eqv trigger i/p */ 
      TM_clbsy(CHAN_ONE);		/* clear pending i/r */
      TM_bimcrW(CHAN_ONE,DIS_BIMCR0);
      TM_bimvrW(CHAN_ONE,VEC_NUM0);
      TM_bimcrW(CHAN_ONE,INI_BIMCR0);
/*	TM_csrW(CHAN_ONE,INI_CSR1);*/	/* enable triggers : after linking */

/* clear from out to in, enable from in to out */
      TM_csrW(CHAN_TWO,DIS_CSR1);      /* disable (int) busy eqv trigger i/p */ 
      TM_clbsy(CHAN_TWO);		/* clear pending i/r */
      TM_bimcrW(CHAN_TWO,DIS_BIMCR0);
      TM_bimvrW(CHAN_TWO,VEC_NUM1);
      TM_bimcrW(CHAN_TWO,INI_BIMCR0);
/*	TM_csrW(CHAN_TWO,INI_CSR1);*/	/* enable triggers : after linking */

/* now the CORBO hardware is ready */
    istat = gpih_open("/dev/gpih",&pathnum);
    if(istat != 0) {
      printf(" gpih_open error %d errno = %d :",istat, errno);
      exit(0);
    }
    else
      printf(" Opened path number %4d\n",pathnum);

    vecnum0 = VEC_NUM0;signum0 = SIGNO0;
    istat = gpih_link(pathnum,GPIH_VME,vecnum0,signum0,&linkid0);
    if (istat != 0) {
      printf(" gpih_link error %d errno = %d \n", istat, errno);
      exit(0);
    }

    printf(" gpih_link on first link : status =%4d\n",istat);
    printf(" vect =%4d signal # =%6d\n",vecnum0,signum0);
    printf(" linkid =%4d\n",linkid0);

    vecnum1 = VEC_NUM1;signum1 = SIGNO1;
    istat = gpih_link(pathnum,GPIH_VME,vecnum1,signum1,&linkid1);
    if (istat != 0) {
      printf(" gpih_link error %d errno = %d \n", istat, errno);
      exit(0);
    }

    printf(" gpih_link on second link : status =%4d\n",istat);
    printf(" vect =%4d signal # =%6d\n",vecnum1,signum1);
    printf(" linkid =%4d\n",linkid1);

    while (ich == 'y') {

      corbo_signal_intercept_count = 0;
      corbo_signal_intercept_count0 = 0;
      corbo_signal_intercept_count1 = 0;
      corbo_signal_main_count0 = 0;
      corbo_signal_main_count1 = 0;
      suspend_count = 0;
      corbo_signal_waitinfo_count = 0;
      corbo_signal_pending0 = 0;
      corbo_signal_pending1 = 0;

      printf(" # loops = ");
      nloops = getdec();
      time(&tbeg);

      TM_csrW(CHAN_ONE,INI_CSR1);	/* enable triggers : after linking */
      TM_csrW(CHAN_TWO,INI_CSR1);	/* enable triggers : after linking */

      for ( i=1;i<=nloops;i++) {

#ifdef NO_HANDLER
        sigwaitinfo(&look_for_these, &extra);
        corbo_signal_waitinfo_count++;
#else
        sigsuspend(&empty_mask);   /* wait for any signal */
	suspend_count++;
#endif
        if(control_c)  break;

      } /* for */

      control_c = 0;
      printf(" # suspend counts = %d\n", suspend_count);
      printf(" # signals seen in handler 1 = %d\n",
              corbo_signal_intercept_count0);
      printf(" # signals seen in handler 2 = %d\n",
              corbo_signal_intercept_count1);
      printf(" NB! possible that (#signals1 + #signals2) > (suspend count)\n");
      printf(" since several (pending) interrupts could be handled  \n");
      printf(" for each sigsuspend \n");
      time(&tend);
      t = difftime(tend, tbeg);
      printf(" elapsed time : %.2lf secs \n", (double) t);
      if (t) printf(" # loops per second in channel 1  : %d \n",
              corbo_signal_intercept_count0 / t);
      if (t) printf(" # loops per second in channel 2  : %d \n",
              corbo_signal_intercept_count1 / t);

      printf(" restart  (y/n) : ");
      ich = getfstchar();

    } /* while*/

/*  clean everything */
    istat = gpih_unlink(pathnum,linkid0);
    if (istat != 0) {
      printf(" gpih_unlink error %d errno = %d \n", istat, errno);
      exit(0);
    }
    istat = gpih_unlink(pathnum,linkid1);
    if (istat != 0) {
      printf(" gpih_unlink error %d errno = %d \n", istat, errno);
      exit(0);
    }

    istat = gpih_close(pathnum);
    if (istat == -1)  {
     printf(" Close error %d :",errno);
    }
    else
      printf(" Closed path number %4d\n",pathnum);

    TM_close();

}

int getdec()  /*  get ONE decimal integer */

{
        char sbuf[10];   /* max 9 chars */
        int nfield,nint;

        do {
                fgets(sbuf,10,stdin);
                nfield = sscanf(sbuf,"%d",&nint);
                if (nfield<1) printf(" ??? : ");
           } while (nfield<1);

        return(nint);
}

char getfstchar()  /*  get first character */
		   /* simpler with int !! */	

{
	char sbuf[10];   /* max 9 chars */

		fgets(sbuf,10,stdin);

	return(sbuf[0]);
}
