/************************************************************************/
/*                                                                      */
/* File: corbo_scan.c                                                   */
/*                                                                      */
/* The CORBO address range is scanned using the VME RCC library         */
/* It provides a test of the R/W Safe functions in the library		*/
/* In particular of how the BERRs are handled				*/
/*                                                                      */
/* 14. Dec. 01  J.O.Petersen  created                                   */
/*                                                                      */
/**************** C 2001 - A nickel program worth a dime ****************/

#include <stdio.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"

#define CORBO_VME_ADDRESS 0x700000
#define LOW_ADDRESS 0x0
#define HIGH_ADDRESS 0x104

/************/
int main(void)
/************/
{
  VME_ErrorCode_t ret;
  VME_MasterMap_t master_map;
  int mhandle, func;
  u_int offset, prev_offset, ldata;
  u_char bdata;
  u_short sdata;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  master_map.vmebus_address   = CORBO_VME_ADDRESS;
  master_map.window_size      = 0x1000;
  master_map.address_modifier = VME_A24;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &mhandle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  do
  {
    printf("\n");
    printf("Select a SAFE function :\n");
    printf("   1 VME_ReadSafeUChar\n");
    printf("   2 VME_ReadSafeUShort\n");
    printf("   3 VME_ReadSafeUInt\n");
    printf("   4 VME_WriteSafeUChar\n");
    printf("   5 VME_WriteSafeUShort\n");
    printf("   6 VME_WriteSafeUInt\n");
    printf("   0 EXIT\n");

    printf("\n");
    printf(" function = ");
    func = getdec();

    if (func == 0)
      break;

    printf(" scan CORBO address range from 0x%x to 0x%x\n",
           CORBO_VME_ADDRESS + LOW_ADDRESS,CORBO_VME_ADDRESS + HIGH_ADDRESS);

    if (func == 1 || func == 4)		// byte functions
    {
      prev_offset = 0xffff;
      for (offset = LOW_ADDRESS; offset <= HIGH_ADDRESS ; offset++)
      {
        if (func == 1)
          ret = VME_ReadSafeUChar(mhandle, offset, &bdata);
        else if (func == 4)
          ret = VME_WriteSafeUChar(mhandle, offset, bdata);

        if (ret != VME_SUCCESS)
        {
          if ( ret == VME_BUSERROR)
          {
            if ( offset != (prev_offset+1) )			// hole detected
            {
              if ( prev_offset != 0xffff )				// not the first time
              {
                printf(" Bus Error to offset   = 0x%8x\n",prev_offset);
              }
              printf(" Bus Error from offset = 0x%8x\n",offset);
            }
            prev_offset = offset;
          }
        }
      }
    }

    if (func == 2 || func == 5)		// short functions
    {
      prev_offset = 0xffff;
      for (offset = LOW_ADDRESS; offset <= HIGH_ADDRESS ; offset += 2)
      {
        if (func == 2)
          ret = VME_ReadSafeUShort(mhandle, offset, &sdata);
        else if (func == 5)
          ret = VME_WriteSafeUShort(mhandle, offset, sdata);

        if (ret != VME_SUCCESS)
        {
          if ( ret == VME_BUSERROR)
          {
            if ( offset != (prev_offset+2) )			// hole detected
            {
              if ( prev_offset != 0xffff )				// not the first time
              {
                printf(" Bus Error to offset   = 0x%8x\n",prev_offset);
              }
              printf(" Bus Error from offset = 0x%8x\n",offset);
            }
            prev_offset = offset;
          }
        }
      }
    }

    if (func == 3 || func == 6)		// long functions
    {
      prev_offset = 0xffff;
      for (offset = LOW_ADDRESS; offset <= HIGH_ADDRESS ; offset += 4)
      {
        if (func == 3)
          ret = VME_ReadSafeUInt(mhandle, offset, &ldata);
        else if (func == 6)
          ret = VME_WriteSafeUInt(mhandle, offset, ldata);

        if (ret != VME_SUCCESS)
        {
          if ( ret == VME_BUSERROR)
          {
            if ( offset != (prev_offset+4) )			// hole detected
            {
              if ( prev_offset != 0xffff )				// not the first time
              {
                printf(" Bus Error to offset   = 0x%8x\n",prev_offset);
              }
              printf(" Bus Error from offset = 0x%8x\n",offset);
            }
            prev_offset = offset;
          }
        }
      }
    }

  } while (func != 0);

  ret = VME_MasterUnmap(mhandle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return(1);

}


