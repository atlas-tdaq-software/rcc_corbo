/* file: vmetrig_test1.c 

  This program tests the interrupt part of the RCC VMEbus library/driver using
  as an interrupt source a CORBO interrupt module.

  The program uses from one to four channels of the CORBO to allow testing
  of the grouping of vectors onto one link. The number of channels used is equal
  to the number of vectors defined in VME_InterruptLink.

  The program supports both semaphores & signals as selected by the user 

  About signals: there are many ways of handling signals in an application!
  This test program shows an example which is "semi" synchronous: the signals
  are blocked everywhere EXCEPT when sigsuspend is called.

  The program also measures the number of interrupts handled per second.

  The VME_InterruptInfoGet is tested explicitly via messages before and after a "run"

  An option is added to simulate processing after an interrupt occured:
  This is to study how processes co-exist when driven by interrupts.

  96/07/08  J.O.Petersen ECP
  000121    adapt to IOM/Atlas
  000124    generalise to any CORBO channel 
  01/11/08  port to RCC 
  01/11/19  merge semaphore & signal versions 
  01/12/13  add processing simulation 
  02/04/18  support for RORA 
  02/05/29  new IR API 

*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "DFDebug/DFDebug.h"
#include "rcc_error/rcc_error.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/vme_rcc.h"
#include "rcc_corbo.h"

// CORBO definitions - see the CORBO manual for details ..

// (internal) busy enabled, busy latch, front panel input, internal busy out,
// count triggers, disable external counter clear, pushbutton resets BIM
#define INI_CSR1 0xc0

// (internal) busy disabled, busy latch, front panel input, internal busy out,
// count triggers, disable external counter clear, pushbutton resets BIM
#define DIS_CSR1 0xc1

// interrupt enable (IRE), autoclear, level 0
#define INI_BIMCR0 0x18

// interrupt disable (level 0)
#define DIS_BIMCR0 0x00

#define VME_VECTOR_DEF 130
#define SIGNUM 42 		// Qed RT signal

static sigset_t empty_mask;
static sigset_t corbo_mask;
static struct sigaction sa;

static int ini_bimcr0 = INI_BIMCR0;
static int corbo_signal_intercept_count = 0;
static int corbo_signal_waitinfo_count = 0;
static int corbo_signal_pending = 0;      // flag : 0 or 1
static int suspend_count = 0;
static int control_c = 0;
static int verbose;

// this refers to the use of sigwaitinfo to avoid a signal handler
// see Gallmeister POSIX.4, p.457 and the POSIX 1003.1 standard, section 3.3
#ifdef NO_HANDLER
  static sigset_t look_for_these;
  static siginfo_t extra;
  static int nohd_signum;
#endif


/*****************************/
void sigint_handler(int signum)
/*****************************/
{
  if (signum == SIGINT)
  {
    if (verbose)
      printf(" sigint_handler: signal # = %d\n", signum);
    
    control_c = 1;
  }
  else 
    printf("unexpected signal\n");
}


/***********************************/
void corbo_signal_handler(int signum)
/***********************************/
{
  if (verbose)
    printf("corbo_signal_handler: CORBO signal\n");

  if (signum == SIGNUM)
  {
    corbo_signal_intercept_count++;
    corbo_signal_pending = 1;
  }
  else 
    printf("unexpected signal\n");
}


/************/
int main(void)
/************/
{
  VME_ErrorCode_t ret;
  int i, j, k, l, found_flag, no_chan, cur_chan, first_chan, int_handle, timeout = -1, int_level, int_mode, nloops; 
  unsigned int io_base_add = 0x700000;		// canonical CORBO VME address
  char ich = 'y', ichstr_sig[10], ichstr_processing[10];
  float delta, usec_per_int;
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;
  tstamp ts1, ts2;
 
  printf(" Verbose mode? 0=no, 1=yes 2=DFDebug  ");
  verbose = getdecd(0);
  if(verbose == 2)
    DF::GlobalDebugSettings::setup(20, DFDB_RCCCORBO);

  printf("\n");
  printf(" using synchronous(semaphores) or asynchronous(signals) (s/a) ");
  getstrd(ichstr_sig, (char *)"s");

  if (ichstr_sig[0] == 'a')	// signals
  {
#ifdef NO_HANDLER
    sigemptyset(&look_for_these);
    sigaddset(&look_for_these, SIGNUM);
    sigaddset(&look_for_these, SIGINT);
#endif

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler =  corbo_signal_handler;
    // dont block any signal in intercept handler
    if (sigaction(SIGNUM, &sa, NULL) < 0) 
    {
      perror(" sigaction SIGNUM ");
      exit(1);
    }
 
    // block CORBO signals everywhere EXCEPT in sigsuspend 
    sigaddset(&corbo_mask, SIGNUM);
    sigprocmask(SIG_BLOCK, &corbo_mask, NULL);
  }

  // SIGINT handler
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler =  sigint_handler;
  if (sigaction(SIGINT, &sa, NULL) < 0) 
  {
    perror(" sigaction SIGINT ");
    exit(1);
  }
 
  printf("\nsimulate processing after interrupt (y/n) ");
  getstrd(ichstr_processing, (char *)"n");

  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("ROAK or RORA interrupt (ROAK = %2d  RORA = %2d)", VME_INT_ROAK, VME_INT_RORA);  
  int_mode = getdecd(VME_INT_ROAK);

  printf("Interrupt level = ");
  int_level = getdecd(3);
  ini_bimcr0 = INI_BIMCR0 + int_level;

  ret = TM_open(io_base_add);
  if(ret != CBE_OK)
    rcc_error_print(stdout, ret);

  printf("First CORBO channel ");
  first_chan = getdecd(1);

  printf("# CORBO channels ");
  no_chan = getdecd(1);

  irq_list.number_of_items = no_chan;		// one vector per channel
  for (i = 0; i < irq_list.number_of_items; i++)
  {
    printf("vector = ");
    irq_list.list_of_items[i].vector = getdecd(VME_VECTOR_DEF + i);
    irq_list.list_of_items[i].level = int_level;
    irq_list.list_of_items[i].type = int_mode;
  }

  if (ichstr_sig[0] == 's')	// semaphores
  {
    printf("timeout ( >0: in ms, 0: to fall through, -1: infinite) = ");
    timeout = getdecd(10000);
  }

  for (i = 0; i < no_chan; i++)
  {
    // clear from out to in, enable from in to out 
    TM_csrW(first_chan + i, DIS_CSR1);	// disable (int) busy eqv trigger i/p 
    TM_clbsy(first_chan + i);		// clear pending i/r 
    TM_bimcrW(first_chan + i, DIS_BIMCR0);
    TM_bimvrW(first_chan + i, irq_list.list_of_items[i].vector);
    TM_bimcrW(first_chan + i, ini_bimcr0);
  }

  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" Interrupt Handle = %d\n", int_handle);

  if (ichstr_sig[0] == 'a')	// signals
  {
    ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

  while (ich == 'y')
  {
    if (ichstr_sig[0] == 'a')	// signals
    {
      corbo_signal_intercept_count = 0;
      corbo_signal_waitinfo_count = 0;
      corbo_signal_pending = 0;
      suspend_count = 0;
    }    

    printf("# loops = ");
    nloops = getdecd(1000);

    ret = ts_clock(&ts1);
    if (ret)
      rcc_error_print(stdout, ret);

    printf("There should be NO interrupt pending: \n");
    ret = VME_InterruptInfoGet(int_handle, &ir_info);	// should be NO IR
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
    printf("It's all OK if you saw the error message\n");

    if (int_mode == VME_INT_RORA)	// the IR may be in a disabled state 
    {
      ret = VME_InterruptReenable(int_handle);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      printf("Interrupts have been re-enabled\n");
    }

    for (i = 0; i < no_chan; i++)
      TM_csrW(first_chan + i, INI_CSR1); 	// (re)enable triggers

    printf("Corbo channel(s) have been re-enabled\n");

    for (i = 1; i <= nloops; i++) 
    {
      if (ichstr_sig[0] == 's')	//semaphores 
      {
        if (verbose)
          printf("Waiting for IRQ\n");
        ret = VME_InterruptWait(int_handle, timeout, &ir_info);
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);

        if (verbose)
          printf("after Wait, level = %d, type = %d, vector = %d\n", ir_info.level, ir_info.type, ir_info.vector);

        if (control_c)
        {
          printf(" CTL/C seen\n");
          control_c = 0;
          break;
        }
      }
      else		// signals
      {
#ifdef NO_HANDLER
        sigwait(&look_for_these, &nohd_signum);
        corbo_signal_waitinfo_count++;
#else
        sigsuspend(&empty_mask);   // wait for any signal
        suspend_count++;
#endif

        if (control_c)
        {
          printf(" CTL/C seen\n");
          control_c = 0;
          break;
        }

        if (corbo_signal_pending)
        {
          ret = VME_InterruptInfoGet(int_handle, &ir_info);	// ACK the interrupt
          if (ret != VME_SUCCESS)
            VME_ErrorPrint(ret);

          if (verbose)
            printf("After InfoGet, level = %d, type = %d, vector = %d\n", ir_info.level, ir_info.type, ir_info.vector);

          corbo_signal_pending = 0;
        }
      }

      if (ret == VME_SUCCESS)		// not if TIMEOUT
      {
        found_flag = 0;
        // find channel that fired so we can reset it
        for (j = 0; j < no_chan; j++)
        {
          if (ir_info.vector == irq_list.list_of_items[j].vector)
          {
            cur_chan = first_chan + j;
            found_flag = 1;
            break;
          }
        }

        if (found_flag == 0)		        // sanity check
        {
          printf(" illegal vector = %d\n", ir_info.vector);
          exit(-1);
        }

        if (ichstr_processing[0] == 'y')	// do some processing
        {
          for (k = 0; k < 1000000; k++)		// about 25 ms (VMEtro)
            l = (7 * k) / 5;
        }

        TM_csrW(cur_chan, DIS_CSR1);	 // disable (int) busy eqv trigger i/p  
        TM_clbsy(cur_chan);	 	 // triggers may arrive, but disabled  
        TM_bimcrW(cur_chan, ini_bimcr0); // reenable IE (BIM) 
        
	if(i != nloops)
	{
	  printf("i = %d, nloops = %d\n", i, nloops);
	  TM_csrW(cur_chan, INI_CSR1); 	 // (re)enable triggers 
        }
	else
	  printf("Last interrupt. IRQ will not be re-enabled\n");
	  
        if (int_mode == VME_INT_RORA)	 // the IR was disabled in the IRQ handler 
        {
          ret = VME_InterruptReenable(int_handle);
          if (ret != VME_SUCCESS)
            VME_ErrorPrint(ret);
        }
      }
    }  // for

    ret = ts_clock(&ts2);
    if (ret)
      rcc_error_print(stdout, ret);
    delta = ts_duration(ts1, ts2);
    printf("Elapsed time : %.1f secs \n", delta);
    usec_per_int = (delta / nloops) * 1000000;
    printf("\n");
    printf("# microsecs per interrupt = %6.2f\n", usec_per_int);
    printf("\n");

    if (ichstr_sig[0] == 'a')	// signals 
    {
      printf(" # signals seen in handler = %d\n", corbo_signal_intercept_count);
      printf(" # signals seen after waitinfo = %d\n", corbo_signal_waitinfo_count);
      printf(" # suspend calls = %d\n", suspend_count);
    }

    printf("Restart  (y/n) : ");
    ich = getfstchar();
  } // while

  //  clean everything
  if (int_mode == VME_INT_RORA)	// the IR was disabled in the IRQ handler 
  {
    ret = VME_InterruptReenable(int_handle);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

  printf("There is MAYBE an interrupt pending: \n");
  ret = VME_InterruptInfoGet(int_handle, &ir_info);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("After InfoGet, level = %d, type = %d, vector = %d, multiple = %d\n", ir_info.level, ir_info.type, ir_info.vector, ir_info.multiple);
  printf("and MAYBE another one: \n");
  ret = VME_InterruptInfoGet(int_handle, &ir_info);	// should be NO IR
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("After InfoGet, level = %d, type = %d, vector = %d, multiple = %d\n", ir_info.level, ir_info.type, ir_info.vector, ir_info.multiple);

  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = TM_close();
  if(ret != CBE_OK)
    rcc_error_print(stdout, ret);
  else
    printf(" TM_Close OK\n");

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  return 1;
}
