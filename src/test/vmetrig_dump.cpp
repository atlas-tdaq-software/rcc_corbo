/****************************************************************/
/* file: vmetrig_dump.c						*/
/*								*/
/* VME trigger module dump program 				*/
/* 								*/
/*   29. Jun. 92  J.O.Petersen ECP/DS				*/
/*   22. Mar. 94  MAJO test routine "regtest" added		*/
/*   21. Jan. 00  JOP  IOM compatible				*/
/*    5. Dez. 00  MAJO common source for VME and PCI CORBO	*/
/****************************************************************/

#include <stdlib.h>
#include <sys/types.h>
#include "rcc_corbo.h"

//globals
int chan_no;
int tmdata;

/****************/
void regtest(void)
/****************/
{
  int offset;
  u_int ret;

  for (offset = 0; offset <= 0x9e; offset += 2) 
  {
    ret = TM_ramW(offset, 0xffff);  
    ret = TM_ramR(offset, &tmdata);
    if (tmdata!=0xffff && ret==0) printf("ERROR! RAM offset: %x  written: 0xffff  read: %x\n", offset, tmdata);
    ret = TM_ramW(offset, 0x0000);  
    ret = TM_ramR(offset, &tmdata);
    if (tmdata!=0 && ret==0) printf("ERROR! RAM offset: %x  written: 0x0000  read: %x\n", offset, tmdata);
  }
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  err_type c_stat;
  int chan_no, offset, tmdata;
  u_int io_base_add=0x700000;

  if (argc != 1)
  {
    printf("This is Version 1.2 of VMETRIG_DUMP\n");
    exit(0);
  }

  //Address is dummy for PCI version
  c_stat = TM_open(io_base_add);
  if(c_stat)
    rcc_error_print(stdout, c_stat);
  else
    printf(" TM_Open OK\n");

  for (chan_no = 1; chan_no <= 4; chan_no++) 
  {
    printf(" TM channel %d : \n", chan_no);
    TM_csrR(chan_no, &tmdata);
    printf("   CSR        = %x\n", tmdata);
    TM_cntR(chan_no, &tmdata); 
    printf("   counter    = %d\n", tmdata);
    TM_toR(chan_no, &tmdata);
    printf("   timeout    = %d\n", tmdata);
    TM_bimcrR(chan_no, &tmdata);
    printf("   BIM CSR    = %x\n", tmdata);
    TM_bimvrR(chan_no, &tmdata);
    printf("   BIM vector = %d\n", tmdata);
  }

  printf("\n");
  for (offset = 0; offset <= 8; offset += 2) 
  {
    c_stat=TM_ramR(offset,&tmdata);
    if(!c_stat)
      printf(" TM RAM address  = %x  data = %x\n", offset, tmdata);
  }
  printf(" etc ...... \n");
  regtest();

  //Address is dummy for PCI version
  c_stat = TM_close();
  if(c_stat)
    rcc_error_print(stdout, c_stat);
  else 
    printf(" TM_Close OK\n");

  return 0;
}

